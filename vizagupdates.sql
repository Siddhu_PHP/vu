-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2018 at 04:09 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vizagupdates`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_languages`
--

CREATE TABLE `tbl_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modules`
--

CREATE TABLE `tbl_modules` (
  `module_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paper`
--

CREATE TABLE `tbl_paper` (
  `paper_id` int(11) NOT NULL,
  `paper_title` varchar(255) DEFAULT NULL,
  `paper_url` varchar(255) DEFAULT NULL,
  `paper_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `paper_position` int(11) NOT NULL,
  `paper_logo` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `paper_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_paper`
--

INSERT INTO `tbl_paper` (`paper_id`, `paper_title`, `paper_url`, `paper_category_id`, `language_id`, `paper_position`, `paper_logo`, `created_on`, `updated_on`, `paper_status`) VALUES
(1, 'paer1111', 'ttt', 2, 0, 3, '8b307b878af5ff6464a6c07ee9fe7f4b.jpg', '2016-07-03 11:57:34', NULL, 3),
(2, 'abc', 'qwe', 11, 0, 0, '804557ee6f8b9a6ba50d6429b0f7beef.jpg', '2016-11-24 08:55:38', NULL, 3),
(3, 'thth', 'thththth', 15, 0, 0, '396469529c32f51d14651e4744c39390.jpg', '2016-11-24 09:55:39', '2016-11-24 16:55:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paper_categories`
--

CREATE TABLE `tbl_paper_categories` (
  `paper_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_paper_categories`
--

INSERT INTO `tbl_paper_categories` (`paper_category_id`, `category_name`, `created_on`, `updated_on`, `status`) VALUES
(1, 'pcat111', '2016-07-03 11:56:06', '2016-07-03 08:26:13', 3),
(2, 'pcat', '2016-07-03 11:56:21', NULL, 1),
(3, 'test', '2016-07-07 09:53:03', NULL, 3),
(4, 'test', '2016-07-07 09:54:15', NULL, 1),
(5, 'test', '2016-07-07 10:02:49', NULL, 1),
(6, 'tst', '2016-07-07 10:03:02', NULL, 1),
(7, 'tert', '2016-07-07 10:03:41', NULL, 1),
(8, 'tytry', '2016-07-07 10:03:55', NULL, 1),
(9, 'testqwr', '2016-07-07 10:06:08', '2016-11-24 16:33:38', 1),
(10, 'long time', '2016-11-24 05:09:53', NULL, 3),
(11, 'long time122', '2016-11-24 05:16:24', NULL, 3),
(12, 'long time122', '2016-11-24 05:16:34', NULL, 3),
(13, 'fggheh', '2016-11-24 05:52:33', NULL, 1),
(14, 'yoyoy', '2016-11-24 05:52:48', '2016-11-24 16:33:15', 1),
(15, 'rgrgrgrg', '2016-11-24 09:36:30', '2016-11-24 16:36:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rolepermissions`
--

CREATE TABLE `tbl_rolepermissions` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `view_` varchar(20) NOT NULL,
  `edit_` varchar(20) NOT NULL,
  `create_` varchar(20) NOT NULL,
  `delete_` varchar(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `role_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`role_id`, `name`, `status`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'developer', 1, '2016-10-04 16:18:34', '2016-10-06 14:44:23', 1, 1),
(2, 'siddhu', 1, '2016-10-04 18:24:12', NULL, 1, 0),
(3, 'fgdg', 1, '2016-11-24 06:17:10', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE `tbl_services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(255) DEFAULT NULL,
  `service_url` varchar(255) DEFAULT NULL,
  `service_category_id` int(11) NOT NULL,
  `service_position` int(11) NOT NULL,
  `service_logo` varchar(255) DEFAULT NULL,
  `service_description` text NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `service_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`service_id`, `service_title`, `service_url`, `service_category_id`, `service_position`, `service_logo`, `service_description`, `created_on`, `updated_on`, `service_status`) VALUES
(1, 'ttt111', 'ttt', 1, 1, '9df58abc38d49bb04a63df98408c7b6c.jpg', 'test', '2016-07-03 11:38:42', NULL, 3),
(2, 'ttt', 'ttt', 2, 1, 'eac53bce40d8e0667a221c58d8810a72.jpg', 'xxxx', '2016-07-03 12:13:44', NULL, 3),
(3, 'test', 'google.com', 4, 2, '360e94717081f3d42e984abe02009c7b.jpg', 'wrgwg', '2016-08-15 12:30:13', NULL, 1),
(4, 'rtheth', 'htrhrth', 4, 45, '9c8a6fc159f7a9e37f4c79e776243de0.jpg', 'hrthrthrth', '2016-08-15 13:06:34', NULL, 1),
(5, 'siddhu', 'siddhui', 5, 5, '7d945905672eab3c8018a81d7d8686d4.jpg', 'rgtergberger', '2016-08-26 05:08:19', NULL, 3),
(6, 'erer', 'ertert', 5, 0, 'b963f810c9a422daa096853ff6379f9c.jpg', 'ertreteter', '2016-08-26 05:10:12', NULL, 1),
(7, 'wdedf', 'ewfefwef', 5, 3, 'bc01ac8e7ba5837141fe54d99d3aa23c.jpg', 'ewwefweef', '2016-08-26 05:11:21', NULL, 3),
(8, 'werfwer', 'werwerwer', 5, 3, '514b917447be2e42e2863d11d9a3c6ce.jpg', 'werwerwerwerwer', '2016-08-26 05:11:45', NULL, 3),
(9, 'efefwef', 'wefwefwef', 4, 7, '2c4eff65dda27d214cd7250cfc2842e3.png', 'asfefasfawf', '2016-08-26 05:12:45', NULL, 3),
(10, 'tyrty siddhu', 'dhdfhgh', 4, 0, '9f1f60ee7ac299a850b32a1f5af600dc.jpg', 'fghfgh', '2016-11-20 07:13:34', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_categories`
--

CREATE TABLE `tbl_service_categories` (
  `service_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service_categories`
--

INSERT INTO `tbl_service_categories` (`service_category_id`, `category_name`, `created_on`, `updated_on`, `status`) VALUES
(1, NULL, '2016-07-03 11:28:40', '2016-11-23 20:46:55', 0),
(2, NULL, '2016-07-03 12:13:12', '2016-11-23 20:47:05', 1),
(3, 'test', '2016-07-07 02:55:35', '2016-11-24 16:00:41', 1),
(4, 'test', '2016-07-07 09:52:41', '2016-11-24 09:25:10', 1),
(5, '6u6uu', '2016-08-26 05:07:35', '2016-11-24 16:00:29', 1),
(6, 'ggrrg', '2016-10-10 02:58:36', '2016-11-24 09:47:27', 1),
(7, NULL, '2016-11-23 13:44:16', NULL, 3),
(8, 'eyety', '2016-11-23 13:59:11', '2016-11-24 16:00:17', 1),
(9, 'yjyj', '2016-11-24 09:30:09', NULL, 1),
(10, 'mg', '2016-11-24 09:34:53', '2016-11-24 16:35:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_roles`
--

CREATE TABLE `tbl_user_roles` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video_categories`
--

CREATE TABLE `tbl_video_categories` (
  `video_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_video_categories`
--

INSERT INTO `tbl_video_categories` (`video_category_id`, `category_name`, `created_on`, `updated_on`, `status`) VALUES
(1, 'video11', '2016-07-03 12:05:40', '2016-07-03 08:35:45', 3),
(2, 'video1', '2016-07-03 12:05:56', '2016-07-03 08:36:01', 3),
(3, 'video1', '2016-07-03 12:18:03', NULL, 1),
(4, 'test', '2016-07-07 09:56:29', NULL, 1),
(5, 'sdfeg', '2016-11-24 11:44:59', '2016-11-24 18:51:06', 3),
(6, 'siddhu', '2016-11-24 11:49:09', '2016-11-24 18:50:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video_channels`
--

CREATE TABLE `tbl_video_channels` (
  `video_id` int(11) NOT NULL,
  `video_title` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `video_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `video_position` int(11) NOT NULL,
  `video_logo` varchar(255) DEFAULT NULL,
  `video_status` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT NULL,
  `video_description` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_video_channels`
--

INSERT INTO `tbl_video_channels` (`video_id`, `video_title`, `video_url`, `video_category_id`, `language_id`, `video_position`, `video_logo`, `video_status`, `created_on`, `updated_on`, `video_description`) VALUES
(1, 'fff', 'fff', 2, 0, 3, '139ff3bd1de0871018e068a60118b2f5.jpg', 3, '2016-07-03 12:07:21', '2016-07-03 08:38:09', 'dfbcbc'),
(2, 'erttyutytyutyutyu', 'erterttyutyutyutyutyu', 6, 0, 3, 'ef527723efc3a587819acd5726fec8fc.jpg', 1, '2016-07-07 10:11:40', '2016-11-24 19:18:19', 'erteryutyutyutyutyutyutyutyu'),
(3, 'egg sid', 'rgrg', 6, 0, 0, '3e412af036f552496324a2556ab796cf.jpg', 3, '2016-11-24 12:01:46', '2016-11-24 19:11:50', 'rgrgrg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `user_role` int(11) NOT NULL COMMENT 'based on user roles',
  `created_on` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `activation_key` varchar(255) NOT NULL,
  `f_name` varchar(55) NOT NULL,
  `l_name` varchar(55) NOT NULL,
  `phone_number` varchar(55) NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `register_by` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `gender`, `picture`, `user_role`, `created_on`, `status`, `ip_address`, `activation_key`, `f_name`, `l_name`, `phone_number`, `updated_on`, `created_by`, `updated_by`, `register_by`) VALUES
(30, 'siddhu', 'siddhu.php@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2016-10-02 03:29:06', 0, '49.205.238.237', 'vizagupdates-active161002032906445', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(29, 'Siddhu Siddhartha Roy', 'siddhu.08pc1a0413@gmail.com', '', 'male', '160801133759profile.jpg', 2, '2016-08-01 13:37:59', 1, '49.205.238.237', 'vizagupdates-active160801133759778', '', '', '', '0000-00-00 00:00:00', 0, 0, 'facebook'),
(31, 'siddhartha esunuri', 'siddharthaesunuri@gmail.com', '', 'male', '161203081228profile.jpg', 2, '2016-12-03 08:12:28', 1, '49.205.238.237', 'vizagupdates-active161203081228253', '', '', '', '0000-00-00 00:00:00', 0, 0, 'google'),
(32, 'Naveen Esumuri', 'esumuri@gmail.com', '', 'male', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s320x320/13502113_976878452428768_8024091603859301589_n.jpg?oh=72c40afd37d59fdd158e38695108987e&oe=590DC8BA', 2, '2017-01-26 07:16:25', 1, '49.205.238.237', 'vizagupdates-active170126071625574', '', '', '', '0000-00-00 00:00:00', 0, 0, 'facebook'),
(33, 'Yashoda Puli', NULL, '', 'female', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s480x480/12115870_120044135018862_3040822636312201443_n.jpg?oh=f8f3ed6e859185ccf4a901aeacb1c63c&oe=5902CA11', 2, '2017-01-26 07:55:28', 1, '49.205.238.237', 'vizagupdates-active170126075528337', '', '', '', '0000-00-00 00:00:00', 0, 0, 'facebook'),
(34, 'Nagababu Cheekatla', 'nagababu.cheekatla@gmail.com', '', 'male', '170127152738profile.jpg', 2, '2017-01-27 15:27:38', 1, '49.205.238.237', 'vizagupdates-active170127152738101', '', '', '', '0000-00-00 00:00:00', 0, 0, 'facebook'),
(35, 'Sivendra Kumar Iglesias', 'kappirisivendrakumar@gmail.com', '', 'male', '170127153613profile.jpg', 2, '2017-01-27 15:36:13', 1, '49.205.238.237', 'vizagupdates-active17012715361338', '', '', '', '0000-00-00 00:00:00', 0, 0, 'facebook'),
(36, 'Sivendra Kumar', 'sivendrajustin@gmail.com', '', '', '170127162806profile.jpg', 2, '2017-01-27 16:28:06', 1, '49.205.238.237', 'vizagupdates-active170127162806652', '', '', '', '0000-00-00 00:00:00', 0, 0, 'google'),
(37, 'Sivendra Kumar Colour Moon Technologies', 'team5@thecolourmoon.com', '', '', '170127170433profile.jpg', 2, '2017-01-27 17:04:33', 1, '49.205.238.237', 'vizagupdates-active170127170433584', '', '', '', '0000-00-00 00:00:00', 0, 0, 'google'),
(38, 'test', 'test@mail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-02-04 11:37:59', 0, '49.205.238.237', 'vizagupdates-active170204113759626', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(39, 'test', 'test@maiil.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-02-04 11:42:26', 0, '49.205.238.237', 'vizagupdates-active170204114226100', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(40, 'test', 'tes2t@mail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-02-19 04:58:45', 0, '49.205.238.237', 'vizagupdates-active170219045845838', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(41, 'adsfsdf', 'esumuri@gsmail.com', 'af15d5fdacd5fdfea300e88a8e253e82', '', '', 2, '2017-02-19 05:41:38', 0, '49.205.238.237', 'vizagupdates-active170219054138548', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(42, 'WEFWEWEF', 'esudmuri@gmail.com', '4297f44b13955235245b2497399d7a93', '', '', 2, '2017-02-19 05:55:36', 0, '49.205.238.237', 'vizagupdates-active17021905553679', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(43, 'WEFWEWEF', 'siddhud@mail.com', '4297f44b13955235245b2497399d7a93', '', '', 2, '2017-02-19 06:28:55', 0, '49.205.238.237', 'vizagupdates-active170219062855961', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(44, 'siddhu', 'superadsmin@mail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-02-19 06:42:57', 0, '49.205.238.237', 'vizagupdates-active170219064257146', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(45, 'WEFWEWEF', 'siddshu@mail.com', '4297f44b13955235245b2497399d7a93', '', '', 2, '2017-02-19 06:45:28', 0, '49.205.238.237', 'vizagupdates-active170219064528633', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(46, 'WEFWEWEF', 'sidsdshu@mail.com', '4297f44b13955235245b2497399d7a93', '', '', 2, '2017-02-19 06:49:04', 0, '49.205.238.237', 'vizagupdates-active170219064904396', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(47, 'siddhu', 'siddhu@qmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-03-12 05:07:49', 0, '49.205.238.237', 'vizagupdates-active170312050749225', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(48, 'Siddhuartha', 'siddhu1.php@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-04-02 04:59:21', 0, '49.205.238.237', 'vizagupdates-active170402045921977', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(49, 'Siddhartha', 'siddhu@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-05-03 04:16:58', 1, '125.62.194.242', 'vizagupdates-active170503041658358', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(50, 'siddhu', 'siddhu1@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-05-15 14:40:02', 1, '106.51.213.158', 'vizagupdates-active170515144002992', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual'),
(51, 'siddhu', 'siddu@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '2017-05-21 04:40:03', 0, '106.51.206.24', 'vizagupdates-active170521044003646', '', '', '', '0000-00-00 00:00:00', 0, 0, 'manual');

-- --------------------------------------------------------

--
-- Table structure for table `vu_locations`
--

CREATE TABLE `vu_locations` (
  `location_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `info` text NOT NULL,
  `lat` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL,
  `address` text NOT NULL,
  `video` text,
  `row_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `url_slug` varchar(255) NOT NULL,
  `telugu` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_desc` text,
  `meta_keywords` text,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vu_locations`
--

INSERT INTO `vu_locations` (`location_id`, `name`, `description`, `info`, `lat`, `longitude`, `address`, `video`, `row_order`, `status`, `url_slug`, `telugu`, `meta_desc`, `meta_keywords`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'tested2', '<p>tested2</p>', '<p>tested2</p>\r\n', '', '', 'Vizag, Andhra Pradesh, India', 'tested2', 0, 1, 'tested2', '<p>tested2</p>\r\n', 'tested2', 'tested2', 1, '2018-01-22 07:44:02', 1, '2018-01-22 07:58:11'),
(2, 'tested1', '<p>tested1</p>', '<p>tested1</p>\r\n', '', '', 'Vizag, Andhra Pradesh, India', 'tested1', 0, 1, 'tested1', '<p>tested1</p>\r\n', 'tested1', 'tested1', 1, '2018-01-22 07:45:03', 1, '2018-01-22 07:56:47'),
(3, 'kailsa giri edit', '<p>some test desc edit</p>', '<p>some other info edit</p>\r\n', '17.7305115', '83.3335151', 'Pedda Waltair, Visakhapatnam, Andhra Pradesh, India', 'test video edit', 0, 0, 'kailsa_giri_edit', '<p>మీ అన్ని తీసివేత అభ్యర్థనలు మరియు సమీక్షలు పూర్తి చేయబడ్డాయి మరియు నింపబడ్డాయి. అన్ని పుల్ అభ్యర్థనలను వీక్షించండి edit</p>\r\n', 'test meta desc edit', 'text meta keywords edit', 1, '2018-02-06 20:07:19', 1, '2018-02-06 20:10:44');

-- --------------------------------------------------------

--
-- Table structure for table `vu_location_images`
--

CREATE TABLE `vu_location_images` (
  `location_image_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `defult_image` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vu_location_images`
--

INSERT INTO `vu_location_images` (`location_image_id`, `location_id`, `name`, `image_alt`, `defult_image`) VALUES
(1, 2, 'db45136bc72729d6c3aea2da559506be.png', 'tested tag', 1),
(2, 2, '6320dc16103cde34e9fb1e2f211fb588.png', '', 0),
(3, 2, 'ac1a2c3ad8bf2ef008fb317e1a6e6bcb.png', '', 0),
(4, 1, 'a2f76229077e59dfd816d77102e21cd6.jpg', '', 0),
(5, 3, 'b2b857038d3afa39e588106b439bd220.png', 'Vizagupdates.com', 0),
(7, 3, '13e36fdc4bdcba4cc1eed6f0a530a9ea.png', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_languages`
--
ALTER TABLE `tbl_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`module_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `tbl_paper`
--
ALTER TABLE `tbl_paper`
  ADD PRIMARY KEY (`paper_id`);

--
-- Indexes for table `tbl_paper_categories`
--
ALTER TABLE `tbl_paper_categories`
  ADD PRIMARY KEY (`paper_category_id`);

--
-- Indexes for table `tbl_rolepermissions`
--
ALTER TABLE `tbl_rolepermissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`role_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_services`
--
ALTER TABLE `tbl_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `tbl_service_categories`
--
ALTER TABLE `tbl_service_categories`
  ADD PRIMARY KEY (`service_category_id`);

--
-- Indexes for table `tbl_user_roles`
--
ALTER TABLE `tbl_user_roles`
  ADD PRIMARY KEY (`user_role_id`);

--
-- Indexes for table `tbl_video_categories`
--
ALTER TABLE `tbl_video_categories`
  ADD PRIMARY KEY (`video_category_id`);

--
-- Indexes for table `tbl_video_channels`
--
ALTER TABLE `tbl_video_channels`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vu_locations`
--
ALTER TABLE `vu_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `vu_location_images`
--
ALTER TABLE `vu_location_images`
  ADD PRIMARY KEY (`location_image_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_languages`
--
ALTER TABLE `tbl_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_paper`
--
ALTER TABLE `tbl_paper`
  MODIFY `paper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_paper_categories`
--
ALTER TABLE `tbl_paper_categories`
  MODIFY `paper_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_rolepermissions`
--
ALTER TABLE `tbl_rolepermissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_services`
--
ALTER TABLE `tbl_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_service_categories`
--
ALTER TABLE `tbl_service_categories`
  MODIFY `service_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_user_roles`
--
ALTER TABLE `tbl_user_roles`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_video_categories`
--
ALTER TABLE `tbl_video_categories`
  MODIFY `video_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_video_channels`
--
ALTER TABLE `tbl_video_channels`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `vu_locations`
--
ALTER TABLE `vu_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vu_location_images`
--
ALTER TABLE `vu_location_images`
  MODIFY `location_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
