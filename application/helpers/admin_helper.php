<?php
if(!function_exists('getStatus'))
{
	function getStatus($val)
	{
		$CI =& get_instance();
		if($val==1)
		{
			return '<span class="label label-sm label-success"> Active </span>';
		}
		else
		{
			return '<span class="label label-sm label-danger"> Inactive </span>';
		}		
	}
}

if(!function_exists('get_date'))
{
	function get_date($date,$type)
	{
		if(! isset($date) && empty($date) && ! isset($type) && empty($type))
		{
			return FALSE;
		}
		else
		{
			$type = strtolower($type);
			if($type == 'd')
			{
				return date("M jS, Y, g:i A",strtotime($date));
			}
			elseif($type == 'dt')
			{
				return date("M jS, Y",strtotime($date));
			}
			else
			{
				return FALSE;
			}
		}
	}
}