<?php
class Upload_model extends CI_Model {
    function __construct() {
      
    }
		
   public function upload_img($folder = '', $file = ''){
			
	  		$this->load->library('upload');
       
            // Check if there was a file uploaded
            if (isset($file) && $file != '' )
            {
				  
                // Specify configuration for File 1
                $config['upload_path'] = './uploads/'.$folder.'/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2024';
                $config['max_width']  = '3840';
                $config['max_height']  = '2160'; 
                $config['encrypt_name']  = true; 
				
				             
 
                // Initialize config for File 1 
                $this->upload->initialize($config);
				
							
					if ($this->upload->do_upload($file))
					{
						$data = $this->upload->data();
						 $upload_data['file_name'] =  $data['file_name'];
						 $upload_data['success'] = 1;
						 return $upload_data;
					}
					else
					{
						$upload_data['errors'] = $this->upload->display_errors( '<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>',' </div> ');
						$upload_data['success'] = 0;
						return $upload_data;
					}
				       
                
				
			}
	
    }
	
	
	function _create_thumbnail($fileName,$width,$height) 
    {
		if(isset($fileName) && ! empty($fileName))
		{
			$this->load->library('image_lib');
			$config['image_library'] = 'gd2';
			$config['source_image'] = './uploads/profilepicture/'.$fileName;       
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = $width;
			$config['height'] = $height;
			$config['new_image'] = './uploads/thumb/'.$fileName;               
			$this->image_lib->initialize($config);
			if(!$this->image_lib->resize())
			{ 
				echo $this->image_lib->display_errors();
			}
			else
			{
				return $this->upload->file_name;
			} 
		}
		else
		{
			return '';
		}
    }
	
	
	
	public function multiple_upload($folder = '', $file = '', $name = '')
	{
		$this->load->library('upload');
		$number_of_files_uploaded = count($file);
		$uploadOk = 1;
		
		for ($i = 0; $i < $number_of_files_uploaded; $i++){
			
			//check file type. if faild return error message
			$allowed =  array('gif','png','jpg','jpeg');
			$filename = $_FILES[$name]['name'][$i];
			if(isset($filename) && !empty($filename)) 
			{
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) )
				{
					$upload_data['errors'] =  '<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>File Extention not accepted </div>';
					$upload_data['success'] = 0;
					$uploadOk = 0;
					return $upload_data;
				}
				
				if($_FILES[$name]['size'][$i] > 5242880)//5 MB (size is also in bytes)
				{ 
					$upload_data['errors'] = '<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>File Size should be below 5 MB only acceptable </div>';
					$upload_data['success'] = 0;
					$uploadOk = 0;
					return $upload_data;
				}
				
				
				$image_info = getimagesize($_FILES[$name]["tmp_name"][$i]);
				$image_width = $image_info[0];
				$image_height = $image_info[1];
				if(($image_width  > 3840) && ($image_height > 2160))
				{
					$upload_data['errors'] = '<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>The image you are attempting to upload doesnt fit into the allowed dimensions.</div>';
					$upload_data['success'] = 0;
					$uploadOk = 0;
					return $upload_data;
				}
			}
			
					
		}
		
		
		if($uploadOk == 1)
		{
			for ($i = 0; $i < $number_of_files_uploaded; $i++){
				if(isset($_FILES[$name]['name'][$i]) && !empty($_FILES[$name]['name'][$i])){
				  $_FILES['userfile']['name']     = $_FILES[$name]['name'][$i];
				  $_FILES['userfile']['type']     = $_FILES[$name]['type'][$i];
				  $_FILES['userfile']['tmp_name'] = $_FILES[$name]['tmp_name'][$i];
				  $_FILES['userfile']['error']    = $_FILES[$name]['error'][$i];
				  $_FILES['userfile']['size']     = $_FILES[$name]['size'][$i];
			  
			  
			  
			  
					// Specify configuration for File 1
					$config['upload_path'] = './uploads/'.$folder.'/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size'] = '2024';
					$config['max_width']  = '3840';
					$config['max_height']  = '2160'; 
					$config['encrypt_name']  = true; 
					
					
					
					  $this->upload->initialize($config);
					  if ( ! $this->upload->do_upload())
					  {
						$upload_data['errors'] = $this->upload->display_errors( '<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>',' </div> ');
						$upload_data['success'] = 0;				
					  }
					  else
					  {
						$upload_data[] = $this->upload->data();
						$upload_data['success'] = 1;			
					  }
				}
				
			}
				  return $upload_data;
		}
	}
   
}
?>