<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('roles_model','roles');
				
    }
	
	public function index()
	{
		$this->settemplate->admin('all_users');
		$this->load->view('all_users_js');
	}
	
	public function create_role()
	{
		$this->settemplate->admin('roles');
	}
	
	
}
