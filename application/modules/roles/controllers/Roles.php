<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('role_model','role');		
    }
	
	public function index()
	{
		$data['roles_list'] = $this->role->getALLRoles();
		$this->settemplate->admin('view_allRoles', $data);
		$this->load->view('view_allRoles_js');
	}
	
	
	public function create_role()
	{
		$this->form_validation->set_rules('role', 'Name', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		

	if ($this->form_validation->run($this) == FALSE)
		{
			 echo validation_errors();
		}
		else
		{
			$data = array(
				'name' => $this->input->post('role'),
				'status' => $this->input->post('status'),
				'created_on' => date('Y-m-d H:i:s'),
				'created_by' => 1, //session insert				
				);
			
			$result = $this->role->userrole($data);
			
	
			if($result){
			echo "YES";
			}else{
			echo "NO";
		   }
						
		}	
	}
	
	public function edit_role()
	{
		$this->form_validation->set_rules('role', 'Name', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		if ($this->form_validation->run($this) == FALSE)
		{
			 echo validation_errors();
		}
		else
		{
			$data = array(
				'name' => $this->input->post('role'),
				'status' => $this->input->post('status'),
				'updated_on' => date('Y-m-d H:i:s'),
				'updated_by' => 1, //session insert				
				);
			
			$result = $this->role->userrole($data,['role_id'=>$this->input->post('role_id')]);
			if($result){
			echo "YES";
			}else{
			echo "NO";
		   }
						
		}	
	}
	
	public function role_edit()
	{
		$array = array();
		$role = $this->input->post('role_id');
		if(isset($role) && is_numeric($role)){
			$result = $this->role->getONERole(['role_id'=>$role]);
			echo json_encode($result);
		}
		else
		{
			echo json_encode($array);
		}
	}
	
	
	public function users_list()
	{
		$data['users_list'] = $this->role->getALL();
		$this->settemplate->general('view_allUsers', $data);
		$this->load->view('view_allUsers_js');
	}
	
	
	public function create_user()
	{
		$this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'email','required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone', 'Phone number', 'required'); // numric
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Password', 'required|matches[password]');

		$data['roles_list'] = $this->role->getALLRoles(['status'=>1]);
		
	if ($this->form_validation->run($this) == FALSE)
		{
			$this->settemplate->general('view_adduser',$data);
			$this->load->view('view_adduser_js');
		}
		else
		{
			$data = array(
				'f_name' => $this->input->post('fname'),
				'l_name' => $this->input->post('lname'),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'phone_number' => $this->input->post('phone'),
				'created_on' => date('Y-m-d H:i:s'),
				'created_by' => 1, //session insert				
				'status' => $this->input->post('status'),				
			);
			
			$result = $this->role->save($data);
			if(isset($result)){
				$role_Insert = array(
				'user_id' => $result,
				'role_id' => $this->input->post('role'),
				);
				$insert = $this->role->saveroles($role_Insert);
			}
	
			if($result){
			 $this->session->set_flashdata('msg', '<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
			}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
		   }
			redirect('roles/users_list');					
		}	
	}
	
	
	public function edit_user()
	{
		$id = decode($_GET['id']);
		$this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('user', 'user', 'required');
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		//$this->form_validation->set_rules('email', 'email','required|valid_email|is_unique[sdk_users.email]');
		$this->form_validation->set_rules('phone', 'Phone number', 'required'); // numric
		
		$data['roles_list'] = $this->role->getALLRoles(['status'=>1]);
		$data['user_details'] = $this->role->get_userById($id);
		print_r($data['user_details']);
		
	if ($this->form_validation->run($this) == FALSE)
		{
			$this->settemplate->general('view_edituser',$data);
			$this->load->view('view_edituser_js');
		}
		else
		{
			$data = array(
				'f_name' => $this->input->post('fname'),
				'l_name' => $this->input->post('lname'),
				'phone_number' => $this->input->post('phone'),
				'updated_on' => date('Y-m-d H:i:s'),
				'updated_by' => 1, //session insert							
			);
			
			$result = $this->role->save($data,['user_id' => $this->input->post('user')]);
			
			if(isset($result)){
				$role_Insert = array(
				'user_id' => $this->input->post('user'),
				'role_id' => $this->input->post('role'),
				);
				$insert = $this->role->saveroles($role_Insert,['user_role_id' => $this->input->post('user_role_id')]);
			}

			if($result){
			 $this->session->set_flashdata('msg', '<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
			}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
		   }
			redirect('roles/users_list');					
		}	
	}
	
	public function user_status()
	{
		$user = $this->input->post('user_id');
		$status = $this->input->post('status');
		if(isset($user) && !empty($user)){
			if($status == 0){
				$data = ['status' => 1];
			}else{
				$data = ['status' => 0];
			}
			$result = $this->role->save($data,['user_id' => $user]);
			if(isset($result)){
				echo "YES";
			}else{
				echo "NO";
			}
		}else{
			echo "NO";
		}	
	}
	
	public function create_module()
	{
		$this->form_validation->set_rules('module', 'module', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		if ($this->form_validation->run($this) == FALSE)
		{
			 echo validation_errors();
		}
		else
		{
			$data = array(
				'name' => $this->input->post('module'),
				'status' => $this->input->post('status'),
				);
			
			$result = $this->role->module($data);
			if($result){
			echo "YES";
			}else{
			echo "NO";
		   }
						
		}	
	}
	
	
	public function add_permissions()
	{
		$data['roles_list'] = $this->role->getALLRoles(['status'=>1]);
		$data['modules_list'] = $this->role->getALLModules(['status'=>1]);
		$this->form_validation->set_rules('role', 'Role', 'required');
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->settemplate->general('view_addPermissions',$data);
			$this->load->view('view_addPermissions_js');
		}
		else
		{
			$mid = $_POST['module'];
			for($i=0;$i<count($mid);$i++)
			{
			$create = $this->input->post('create_'.$mid[$i]);
			$edit = $this->input->post('edit_'.$mid[$i]);
			$view = $this->input->post('read_'.$mid[$i]);			 
			$delete = $this->input->post('delete_'.$mid[$i]);			 
			
			$data = array (
				'module' => $mid[$i],
				'create' => ($create)?$create:0,
				'edit' => ($edit)?$edit:0,
				'view' => ($view)?$view:0,
				'delete' => ($delete)?$delete:0,
			);
			print_r($data)."<br/>";
			
			}
	
			/* if($result){
			 $this->session->set_flashdata('msg', '<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
			}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
		   }
			redirect('roles/users_list');	 */				
		}	
	}
}
