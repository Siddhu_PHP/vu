<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*------------------------------------------------
			Admin_model
--------------------------------------------------
Author		:-	Siddhu
Date		:-	12th aug-2016
Data Table	:-	Admin_model
*/
class Role_model extends CI_Model
{
	private $tableName;
	private $Roles;
	private $Modules;
	private $Permissions;
	private $userRoles;
	public function __construct() {
        parent::__construct();
		$this->tableName = 'users';	/*---Assigning table name-----*/	
		$this->Roles = 'tbl_roles';		
		$this->Modules = 'tbl_modules';		
		$this->Permissions = 'tbl_rolepermissions';		
		$this->userRoles = 'tbl_user_roles';		
    }
	
	/*------------------------------------------------------------
						getONE
	--------------------------------------------------------------
	@Author			:	Siddhu
	@Date			:	30th sep-2016
	@Objective		:	To get admin details
	@Params			:	$param(array) contains 1 or more key and values
	@Returns		:	Admin data as an array		
	*/
	public function getONE($param)
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$services = $this->db->limit(1)->get_where($this->tableName,$param);
			if($services->num_rows() > 0)
			{
				return $services->row_array();
			}
		}
		return $array;
	}
	/*----------------getALL-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To get multiple services from database
	*@Params	:	$param(array) contains 0 or more key and values
	*@Return	:	Services(array)
	*/
	public function getALL($param = "")
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$this->db->where($param);			
		}
		$services = $this->db->get($this->tableName);
		if($services->num_rows() > 0)
		{
			foreach($services->result_array() as $service)
			{
				$array[] = $service;
			}
		}
		return $array;
	}
	
	
	/*----------------save-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To save service
	*@Params	:	service data(array)
	*@Return	:	status
	*/
	public function save($data,$con="")
	{
		if(is_array($con))
		{
		return	$this->db->update($this->tableName,$data,$con);			/*Inserting in to database*/
		}
		else
		{
	    	$this->db->insert($this->tableName,$data);	
			 return $this->db->insert_id();
			/*Inserting in to database*/	
		}			
		
	}
	
	/*----------------Delete-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To delete service
	*@Params	:	servicedata(array)
	*@Return	:	status
	*/
	function row_delete($id)
	{
	   $this->db->where('user_id', $id);
	  return $this->db->delete($this->tableName); 
	}
	
	
	
	/*----------------save-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To save service
	*@Params	:	service data(array)
	*@Return	:	status
	*/
	public function userrole($data,$con="")
	{
		if(is_array($con))
		{
		return	$this->db->update($this->Roles,$data,$con);
		}
		else
		{
	    	$this->db->insert($this->Roles,$data);	
			 return $this->db->insert_id();		
		}			
	}
	
	/*----------------getALLRoles-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To get multiple services from database
	*@Params	:	$param(array) contains 0 or more key and values
	*@Return	:	Services(array)
	*/
	public function getALLRoles($param = "")
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$this->db->where($param);			
		}
		$services = $this->db->get($this->Roles);
		if($services->num_rows() > 0)
		{
			foreach($services->result_array() as $service)
			{
				$array[] = $service;
			}
		}
		return $array;
	}
	/*----------------getALLModules-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To get multiple services from database
	*@Params	:	$param(array) contains 0 or more key and values
	*@Return	:	Services(array)
	*/
	public function getALLModules($param = "")
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$this->db->where($param);			
		}
		$services = $this->db->get($this->Modules);
		if($services->num_rows() > 0)
		{
			foreach($services->result_array() as $service)
			{
				$array[] = $service;
			}
		}
		return $array;
	}
	
	
	
	/*------------------------------------------------------------
						getONERole
	--------------------------------------------------------------
	@Author			:	Siddhu
	@Date			:	30th sep-2016
	@Objective		:	To get admin details
	@Params			:	$param(array) contains 1 or more key and values
	@Returns		:	Admin data as an array		
	*/
	public function getONERole($param)
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$services = $this->db->limit(1)->get_where($this->Roles,$param);
			if($services->num_rows() > 0)
			{
				return $services->row_array();
			}
		}
		return $array;
	}
	
	/*----------------Delete-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To delete service
	*@Params	:	servicedata(array)
	*@Return	:	status
	*/
	function role_delete($id)
	{
	   $this->db->where('role_id', $id);
	  return $this->db->delete($this->Roles); 
	}
	
	
	
	/*----------------save-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To save service
	*@Params	:	service data(array)
	*@Return	:	status
	*/
	public function module($data,$con="")
	{
		if(is_array($con))
		{
		return	$this->db->update($this->Modules,$data,$con);
		}
		else
		{
	    	$this->db->insert($this->Modules,$data);	
			 return $this->db->insert_id();		
		}			
	}
	
	/*----------------userRoles-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To save service
	*@Params	:	service data(array)
	*@Return	:	status
	*/
	public function saveroles($data,$con="")
	{
		if(is_array($con))
		{
		return	$this->db->update($this->userRoles,$data,$con);
		}
		else
		{
	    	$this->db->insert($this->userRoles,$data);	
			 return $this->db->insert_id();		
		}			
	}
		
	/*----------------Delete-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To delete service
	*@Params	:	servicedata(array)
	*@Return	:	status
	*/
	function module_delete($id)
	{
	  $this->db->where('module_id', $id);
	  return $this->db->delete($this->Modules); 
	}
	
	/*----------------Delete-----------------------
	*@Author	:	Siddhu
	*@Date		:	12th aug-2016
	*@Objective	:	To delete service
	*@Params	:	servicedata(array)
	*@Return	:	status
	*/
	function get_userById($id)
	{
	 $array = array();
	 $sql = "SELECT su.*,  ur.user_role_id, ur.role_id FROM users su, tbl_user_roles ur WHERE su.user_id = {$id}";	
	 $query = $this->db->query($sql);
	 if($query->num_rows() > 0)
			{
				return $query->result_array();
			}
	 return $array;
	}
	
}
