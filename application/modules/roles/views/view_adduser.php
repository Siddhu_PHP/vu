<div class="page-fixed-main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-layers font-green"></i>
						<span class="caption-subject font-green sbold uppercase">Add Admin user</span>
					</div>
				</div>
				<?php echo validation_errors('<div class="alert alert-warning">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>'); ?>
				<div class="portlet-body">
					<form action="<?php echo base_url('roles/create_user');?>" method="post" id="admin_form">
						<div class="form-body">
						<div class="form-group form-md-line-input form-md-floating-label">
							<select class="form-control" name="role" required>
								<option value="">Select Role</option>
								<?php foreach($roles_list as $data){?>
								<option value="<?php echo $data['role_id'];?>"><?php echo $data['name'];?></option>
								<?php } ?>
							</select>
							<span class="help-block">Please select a role...</span>
						</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" autocomplete="off" name="fname" id="fname">
								<label >First Name</label>
								<span class="help-block">please enter First Name...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" autocomplete="off" name="lname" id="lname">
								<label >Last Name</label>
								<span class="help-block">please enter Last Name...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="email" class="form-control" autocomplete="off" name="email" id="email">
								<label >Email</label>
								<span class="help-block">please enter email...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" autocomplete="off" name="password" id="password">
								<label >Password</label>
								<span class="help-block">please enter Password...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" autocomplete="off" name="cpassword" id="cpassword">
								<label >Confirm Password</label>
								<span class="help-block">please enter Confirm Password...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" autocomplete="off" name="phone" id="phone">
								<label >Phone number</label>
								<span class="help-block">please enter phone number...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
							<select class="form-control" name="status" required>
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
							<span class="help-block">Please select a status...</span>
						</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-success">Save</button>
									<button type="reset" class="btn default">Reset</button>
									<button type="reset" class="btn default" onclick="goBack()">Go Back</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>