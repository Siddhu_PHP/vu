<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
 <div class="page-fixed-main-content">
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet light portlet-fit portlet-datatable bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-settings font-green"></i>
						<span class="caption-subject font-green sbold uppercase">Admin Users</span>
					</div>
					<div class="actions">
						<div class="btn-group btn-group-devided">
						<a href="<?php echo base_url('roles/create_user');?>" class="btn btn-sm green"> Add New <i class="fa fa-plus"></i></a>
						</div>
						<div class="btn-group">
							<a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
								<i class="fa fa-share"></i>
								<span class="hidden-xs"> Trigger Tools </span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right" id="sample_3_tools">
								<li>
									<a href="javascript:;" data-action="0" class="tool-action">
										<i class="icon-printer"></i> Print</a>
								</li>
								<li>
									<a href="javascript:;" data-action="1" class="tool-action">
										<i class="icon-check"></i> Copy</a>
								</li>
								<li>
									<a href="javascript:;" data-action="2" class="tool-action">
										<i class="icon-doc"></i> PDF</a>
								</li>
								<li>
									<a href="javascript:;" data-action="3" class="tool-action">
										<i class="icon-paper-clip"></i> Excel</a>
								</li>
								<li>
									<a href="javascript:;" data-action="4" class="tool-action">
										<i class="icon-cloud-upload"></i> CSV</a>
								</li>
								<li class="divider"> </li>
								<li>
									<a href="javascript:;" data-action="5" class="tool-action">
										<i class="icon-refresh"></i> Reload</a>
								</li>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<table class="table table-striped table-bordered table-hover" id="sample_3">
							<thead>
								<tr>
									<th> FirstName </th>
									<th> LastName </th>
									<th> Email </th>
									<th> Phone Number </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($users_list) > 0) { ?>
								<?php $i=1;foreach($users_list as $data){?>
								<tr>
									<td> <?php echo $data['f_name']?> </td>
									<td> <?php echo $data['l_name']?> </td>
									<td> <?php echo $data['email']?> </td>
									<td> <?php echo $data['phone_number']?> </td>
									<td id="ajax_<?php echo $data['user_id']; ?>"> <?php echo getStatus($data['status']);?> </td>
									<td> 
										<div class="btn-group">
											<button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions<i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-left" role="menu">
												<li>
													<a href="<?php echo base_url('roles/view?id='.encode($data['user_id']));?>"><i class="fa fa-eye"></i> View </a>
												</li>
												<li>
													<a href="<?php echo base_url('roles/edit_user?id='.encode($data['user_id']));?>"><i class="fa fa-edit"></i> Edit </a>
												</li>
												<li id="onajax_<?php echo $data['user_id']; ?>">
													<a href="#" onclick="return statusFunction('<?php echo $data['user_id']; ?>','<?php echo $data['status']; ?>')"><i class="fa fa-cogs"></i> Change Status </a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr><td>Data not available</td></tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
</div>

  