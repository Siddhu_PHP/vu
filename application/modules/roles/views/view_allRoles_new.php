<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
 <div class="page-fixed-main-content">
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet light portlet-fit portlet-datatable bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-settings font-green"></i>
						<span class="caption-subject font-green sbold uppercase">Roles</span>
					</div>
					<div class="actions">
						<div class="btn-group btn-group-devided">
						 <button type="button" class="btn btn-sm green" data-toggle="modal" data-target="#myModal">Add New <i class="fa fa-plus"></i></button>
						</div>
						<div class="btn-group">
							<a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
								<i class="fa fa-share"></i>
								<span class="hidden-xs"> Trigger Tools </span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right" id="sample_3_tools">
								<li>
									<a href="javascript:;" data-action="0" class="tool-action">
										<i class="icon-printer"></i> Print</a>
								</li>
								<li>
									<a href="javascript:;" data-action="1" class="tool-action">
										<i class="icon-check"></i> Copy</a>
								</li>
								<li>
									<a href="javascript:;" data-action="2" class="tool-action">
										<i class="icon-doc"></i> PDF</a>
								</li>
								<li>
									<a href="javascript:;" data-action="3" class="tool-action">
										<i class="icon-paper-clip"></i> Excel</a>
								</li>
								<li>
									<a href="javascript:;" data-action="4" class="tool-action">
										<i class="icon-cloud-upload"></i> CSV</a>
								</li>
								<li class="divider"> </li>
								<li>
									<a href="javascript:;" data-action="5" class="tool-action">
										<i class="icon-refresh"></i> Reload</a>
								</li>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<table class="table table-striped table-bordered table-hover" id="sample_3">
							<thead>
								<tr>
									<th> Roles </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($roles_list) > 0) { ?>
								<?php $i=1;foreach($roles_list as $data){?>
								<tr>
									<td> <?php echo $data['name']?> </td>
									<td> <?php //echo getStatus($data['status']);?> </td>
									<td> 
									<span  data-toggle="modal" data-id="<?php echo $data['role_id'];?>" title="Add this item" href="#open-Edit" class="label label-sm label-success open-Edit"> <i class="fa fa-edit"></i> Edit </span>
									</td>
								</tr>
								<?php } ?>
								<?php }else{ ?>
								<tr><td>Data not available</td></tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
</div>

   <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div id="alert-msg"></div>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Role</h4>
        </div>
		<form action="" name="" method="post">
        <div class="modal-body">
   <div class="form-group row">
  <label for="example-text-input" class="col-xs-2 col-form-label">Name</label>
  <div class="col-xs-10">
    <input class="form-control" type="text"  name="role" id="role">
  </div>
</div>
<div class="form-group row">
  <label for="example-search-input" class="col-xs-2 col-form-label">Status</label>
  <div class="col-xs-10">
    <select class="form-control" name="status" id="status">
	<option value ="1">Active</option>
	<option value ="0">Inactive</option>
	</select>
  </div>
</div></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  <button type="button" id="submit" class="btn green">Save changes</button>
		</div>
		</form>
      </div>
      
    </div>
  </div>
  
  
   <!-- Edit Modal -->
  <div class="modal fade" id="open-Edit" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div id="alert-msg2"></div>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Role</h4>
        </div>
		<form action="" name="" method="post">
        <div class="modal-body">
   <div class="form-group row">
  <label for="example-text-input" class="col-xs-2 col-form-label">Name</label>
  <div class="col-xs-10">
	<input class="form-control" type="hidden" value=""  name="role_id" id="role_id">
    <input class="form-control" type="text" value=""  name="role" id="ename">
  </div>
</div>
<div class="form-group row">
  <label for="example-search-input" class="col-xs-2 col-form-label">Status</label>
  <div class="col-xs-10">
    <select class="form-control" name="status" id="estatus">
	<option value ="1">Active</option>
	<option value ="0">Inactive</option>
	</select>
  </div>
</div></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  <button type="button" id="edit-submit" class="btn green">Save changes</button>
		</div>
		</form>
      </div>
      
    </div>
  </div>
  
 