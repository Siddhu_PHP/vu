<script type="text/javascript">
$('#submit').click(function() {
	 var form_data = {
        role: $('#role').val(),
        status: $('#status').val(),
        };
    $.ajax({
        url: "<?php echo base_url('roles/create_role'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES'){
                $('#alert-msg').html('<div class="alert alert-success text-center">Role added successfully!</div>');
					$('#myModal').modal('hide');
					 toastr.success('Role added successfully!');
					 //location.reload();
				}else if (msg == 'NO'){
                $('#alert-msg').html('<div class="alert alert-danger text-center">Error in adding role! Please try again later.</div>');
            }else{
                $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
        }
    });
    return false;
});

$('#edit-submit').click(function() {
	 var form_data = {
        role: $('#ename').val(),
        status: $('#estatus').val(),
        role_id: $('#role_id').val(),
        };
    $.ajax({
        url: "<?php echo base_url('roles/edit_role'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES'){
                $('#alert-msg2').html('<div class="alert alert-success text-center">Role added successfully!</div>');
					$('#open-Edit').modal('hide');
					 toastr.success('Role updated successfully!');
					 //location.reload();
				}else if (msg == 'NO'){
                $('#alert-msg2').html('<div class="alert alert-danger text-center">Error in adding role! Please try again later.</div>');
            }else{
                $('#alert-msg2').html('<div class="alert alert-danger">' + msg + '</div>');
			}
        }
    });
    return false;
});


$(document).on("click", ".open-Edit", function () {
     var myEditId = $(this).data('id');
	 var form_data = {
        role_id: myEditId,       
        };
	 $.ajax({
        url: "<?php echo base_url('roles/role_edit'); ?>",
        type: 'POST',
        data: form_data,
		datatype:'JSON',
        success: function(data) {
			var obj = jQuery.parseJSON(data);
			$('#role_id').val(obj.role_id);
			$('#ename').val(obj.name);
			$('#estatus').val(obj.status);
        }
    });
	 
    
});


</script>