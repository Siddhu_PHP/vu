<script>
function statusFunction(id,status){
	var form_data = {
        user_id: id,
        status: status,
        };
    $.ajax({
        url: "<?php echo base_url('roles/user_status'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES'){
				if(status == 1){
					$('#ajax_'+id).html('<span class="label label-sm label-danger"> Inactive </span>');
					$('#onajax_'+id).html("<a href='#' onclick='return statusFunction("+id+",0)'><i class='fa fa-cogs'></i> Change Status </a>");
                 }else{
                   $('#ajax_'+id).html('<span class="label label-sm label-success"> Active </span>');
                   $('#onajax_'+id).html("<a href='#' onclick='return statusFunction("+id+",1)'><i class='fa fa-cogs'></i> Change Status </a>");
				}
				}else if (msg == 'NO'){
                alert('Please try again..!');
            }else{
                alert('Please try again..!');
			}
        }
    });
    return false;	
}
</script>