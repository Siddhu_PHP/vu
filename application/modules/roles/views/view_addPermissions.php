<div class="page-fixed-main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-layers font-green"></i>
						<span class="caption-subject font-green sbold uppercase">Add Permissions to Roles</span>
					</div>
				</div>
				<?php echo validation_errors('<div class="alert alert-warning">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>'); ?>
				<div class="portlet-body">
					<form action="" method="post" id="admin_form">
						<div class="form-body">
						<div class="form-group form-md-line-input form-md-floating-label">
							<select class="form-control" name="role" required>
								<option value="">Select Role</option>
								<?php foreach($roles_list as $data){?>
								<option value="<?php echo $data['role_id'];?>"><?php echo $data['name'];?></option>
								<?php } ?>
							</select>
							<span class="help-block">Please select a role...</span>
						</div>
						<?php foreach($modules_list as $data){ ?>
							<div class="form-group form-md-line-input form-md-floating-label">
							<label class="col-md-3 control-label" for="form_control_1"><?php echo $data["name"]; ?></label>
							<div class="col-md-9">
								<div class="md-checkbox-inline">
									<div class="md-checkbox">
										<input type="checkbox" id="create<?php echo $data["module_id"]; ?>" name="create_<?php echo $data["module_id"]; ?>[]" value="1" class="md-check checked_<?php echo $data["module_id"]; ?>">
										<label for="create<?php echo $data["module_id"]; ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Create </label>
									</div>
									<div class="md-checkbox">
										<input type="checkbox" id="edit<?php echo $data["module_id"]; ?>" name="edit_<?php echo $data["module_id"]; ?>[]" value="1" class="md-check checked_<?php echo $data["module_id"]; ?>">
										<label for="edit<?php echo $data["module_id"]; ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Edit </label>
									</div>
									<div class="md-checkbox">
										<input type="checkbox" id="delete<?php echo $data["module_id"]; ?>" name="delete_<?php echo $data["module_id"]; ?>[]" value="1" class="md-check checked_<?php echo $data["module_id"]; ?>">
										<label for="delete<?php echo $data["module_id"]; ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Delete </label>
									</div>
									<div class="md-checkbox">
										<input type="checkbox" id="read<?php echo $data["module_id"]; ?>" name="read_<?php echo $data["module_id"]; ?>[]" value="1" class="md-check checked_<?php echo $data["module_id"]; ?>">
										<label for="read<?php echo $data["module_id"]; ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> Read </label>
									</div>
									<div class="md-checkbox">
										<input type="checkbox" id="all<?php echo $data["module_id"]; ?>" name="all" value="<?php echo $data["module_id"]; ?>" class="md-check allcheck">
										<label for="all<?php echo $data["module_id"]; ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span> All </label>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="module[]" value="<?php echo $data["module_id"]; ?>">
						<?php } ?>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-success">Save</button>
									<button type="reset" class="btn default">Reset</button>
									<button type="reset" class="btn default" onclick="goBack()">Go Back</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>