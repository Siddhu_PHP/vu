<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
  <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo $asset_url;?>backend/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo $asset_url;?>backend/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo $asset_url;?>backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo $asset_url;?>backend/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo $asset_url;?>backend/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
		 <!-- BEGIN PAGE LEVEL PLUGINS -->
       <script src="<?php echo $asset_url;?>backend/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
       <script src="<?php echo $asset_url;?>backend/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
       <!-- END PAGE LEVEL PLUGINS -->
	   <script src="<?php echo $asset_url;?>backend/global/scripts/jquery.validate.min.js" type="text/javascript"></script>