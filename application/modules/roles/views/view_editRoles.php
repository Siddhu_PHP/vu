<div class="page-fixed-main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-layers font-green"></i>
						<span class="caption-subject font-green sbold uppercase">Products</span>
					</div>
				</div>
				<div class="portlet-body">
					<form action="<?php echo base_url('admin/products/e_save');?>" method="post" id="product_form">
						<input type="hidden" name="product_id" id="product_id" value="<?php echo $product['product_id']?>">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" autocomplete="off" name="name" id="name" value="<?php echo $product['name']?>">
								<label >Name</label>
								<span class="help-block">please enter name...</span>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<textarea class="form-control" name="description" rows="3" id="description" ><?php echo $product['description']?></textarea>
								<label >Description</label>
								<span class="help-block">please enter description...</span>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-success">Save</button>
									<button type="reset" class="btn default">Reset</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>