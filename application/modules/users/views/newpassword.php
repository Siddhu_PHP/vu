  <!-- Main Content Start -->
  
  <div class="main-content"> 
  

    
    <!-- Login Page Start -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
		 
          <div class="cp-login-box">
		  
            <div class="text-center"><img style="height:103px; margin-left: 129px;" src="<?php echo base_url();?>/assets/frontend/images/logo.svg" alt=""></div>
             <?php echo validation_errors('<div class="alert alert-warning">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		   <?php echo $this->session->flashdata('forget_msg'); ?>
			<div class="cp-login-form">
              <form class="material" action="<?php echo base_url('users/newpassword');?>"  method="post">
			   <input type="hidden" value= "<?php echo $uid; ?>" name="uid">
                <ul>
                  <li class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" placeholder="new password" name="forget_password">
                  </li>
                  <li class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" placeholder=" confirm Password" name="forget_cpassword">
                  </li>
                  <li>
                    <button type="submit" class="btn btn-submit waves-effect waves-button"> Submit <i class="fa fa-angle-right"></i></button>
                  </li>
                </ul>
              </form>             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Login Page End --> 
    
  </div>
  
  <!-- Main Content End --> 