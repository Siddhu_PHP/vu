<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*------------------------------------------------
			user_model
--------------------------------------------------
Author		:-	Siddhartha
Date		:-	11th JULY-2016
Data Table	:-	users
*/
class User_model extends CI_Model
{
	private $tableName;
	public function __construct() {
        parent::__construct();
		$this->tableName = 'users';
    }
	
	public function getONE($param)
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$user_data = $this->db->limit(1)->get_where($this->tableName,$param);
			if($user_data->num_rows() > 0)
			{
				return $user_data->row_array();
			}		
		}
	}
	
	function validate($data){
		$email = $data['email'];
		$pwd = md5($data['password']);
		$query = $this->db->query("SELECT * FROM $this->tableName WHERE email = '{$email}' AND password = '{$pwd}' AND user_role = '2'"); 
		//user role 2 is user
		if($query->num_rows() === 1){
			return $query->row();
		}else{
			return false;
		}
	}
	
	public function insertUser($data)
	{
		if($this->db->insert($this->tableName, $data))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;   
		}
	}
	
	public function userActivation($data)
	{
		$this->db->where('activation_key',$data['activation_key']);
		if($this->db->update($this->tableName,$data))
		{
			return true;    
		}
	}
	
	public function changePassword($data)
	{
		
		$this->db->where('email',$data['email']);
		if($this->db->update($this->tableName,$data))
		{
			return true;    
		}
	}
	
	function mail_exists($key)
	{
		$this->db->where('email',$key);
		$query = $this->db->get($this->tableName);
		if ($query->num_rows() > 0){
			return false;
		}
		else{
			return true;
		}
	}
	
}
