<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('user_model','user');
		$this->load->model('mails_model','mails');
    }
	
	public function index()
	{
		$this->load->view('register');
	}
	
	
	public function validuser()
	{
		 $this->form_validation->set_rules('email', 'Email', 'required');
         $this->form_validation->set_rules('password', 'Password', 'required');
		 
		  if ($this->form_validation->run($this) == FALSE)
                {
                    //$this->load->view('login');
					 echo validation_errors();
                }
                else 
                {
				  $data = ['email' => $this->input->post('email'), 'password' => $this->input->post('password')];
                  $user = $this->user->validate($data);
				  
		
					if($user != false)
					{
							$data = array(
								'userId' => $user->user_id,
								'email' => $user->email,
								'usertype' => $user->user_role,
								'picture' => $user->picture?base_url('uploads/thumb').'/'.str_ireplace(".jpg","_thumb.jpg",$user->picture):base_url('assets/frontend/custom-images/default_user.png'),
								'is_logged_in' => true
							);
							$this->session->set_userdata($data);
							$this->session->set_flashdata('msg', '<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
							echo 1;
					}else{
							$this->session->set_flashdata('msg', '<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invalid Login Details </div> ');
							//redirect('users/validuser');  
							echo 2;							
						}
                }	
	}
	
	public function register()
	{
		$this->form_validation->set_rules('name', 'Username', 'required');
		//$this->form_validation->set_rules('agree', 'agree', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Password', 'required|matches[password]');

	if ($this->form_validation->run($this) == FALSE)
		{
			$error['name'] =  form_error('name');
			//$error['agree'] =  form_error('agree');
			$error['email'] =  form_error('email');
			$error['password'] =  form_error('password');
			$error['cpassword'] =  form_error('cpassword');
			echo json_encode($error);
		}
		else
		{
			$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'user_role' => 2,
				'created_on' => date('Y-m-d H:i:s'),
				'status' => 0,
				'ip_address' =>  $this->input->ip_address(),
				'activation_key' => 'vizagupdates-active'.date('ymdHis').rand(0,1000),
				'register_by' => 'manual',
			);
			$result = $this->user->insertUser($data);
			
			if($result){
			 $this->mails->sendMail($data['email'],$data['activation_key'],'register');
			 //$this->mails->serverEmail($data['email'],$data['activation_key'],'register');
			 echo "yes";		   
			}
						
		}	
	}
	
	public function dashboard()
	{
		echo "dashboard";
	}
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect('users/validuser');       
		}       
	}
	
	
	function logout(){
		$this->session->sess_destroy();
		redirect('');
	}
	
	
	public function activeUser($uid)
	{
	  $data = array(
			'status' => 1,
			'activation_key'=>$uid
	  );
	  $result = $this->user->userActivation($data);
	  if($result){
			$this->session->set_flashdata('msg', '<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
			}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
			}
			redirect('/home?msg=Activated');	 
	}
	
	public function forgetPassword()
	{
	
			if($this->input->post('forget_email'))
			{
				$data = ['email' => $this->input->post('forget_email')];
				$result = $this->user->getONE($data);
				if($result['user_id'])
				{
					$mail = $this->mails->sendMail($result['email'],$result['activation_key'],'forgetPassword');
					if($mail)
					{
						$this->session->set_flashdata('msg', '<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Instructions to reset your password has been sent to your email. Please check your for these next steps</div>');
					}
					echo 1;
				}
				else
				{
					echo 2;
				}				
			}
			else
			{
				echo 3;
			}
		
		
		
	}
	
	public function newpassword($uid = '')
	{
		
		$this->form_validation->set_rules('forget_password', 'Password', 'required');
		$this->form_validation->set_rules('forget_cpassword', 'Password', 'required|matches[forget_password]');
		if ($this->form_validation->run($this) == FALSE)
		{
			$data = ['uid' => $uid];
			$this->settemplate->home('newpassword',$data);
		}
		else
		{
			$data = ['password' => md5($this->input->post('forget_password')), 'activation_key' => $this->input->post('uid')];
			$result = $this->user->userActivation($data);
			  if($result){
					$this->session->set_flashdata('forget_msg', '<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated. Please login to continue.. </div>');
					}else{
					$this->session->set_flashdata('forget_msg', '<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
					}
					redirect('/users/newpassword/'.$this->input->post('uid'));	
		}
	} 
	
	public function siddhu()
	{
		$this->settemplate->home('newpassword');
	}
	
	
	
}
?>
