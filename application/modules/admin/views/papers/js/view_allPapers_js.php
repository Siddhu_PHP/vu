<script type="text/javascript">
$('#submit').click(function() {
	 var form_data = {
        category: $('#category').val(),
        status: $('#status').val(),
        categoryid: $('#categoryid').val(),
        };
    $.ajax({
        url: "<?php echo base_url('admin/paper/addPaperCategory'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == 'YES'){
                $('#alert-msg').html('<div class="alert alert-success text-center">Role added successfully!</div>');
					$('#myModal').modal('hide');
					// toastr.success('Role added successfully!');
					 location.reload();
				}else if (msg == 'NO'){
                $('#alert-msg').html('<div class="alert alert-danger text-center">Error in adding role! Please try again later.</div>');
            }else{
                $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
        }
    });
    return false;
});
function delthisid(id,del){ 
		 var r = confirm("Are you sure to delete this Paper ?");
		if (r == true) {
			var base_url = "<?php echo base_url(); ?>";
				$.ajax({
						type: 'POST',
						url: base_url+'admin/paper/deletePaper',
						data: {id: id},
						success: function(data) {
							if(data == 1){
								$("#del"+del).empty();
							}
						}
				}); 
		} 
	}
	function delCate(id,del){ 
		 var r = confirm("Are you sure to delete this Category ?");
		if (r == true) {
			var base_url = "<?php echo base_url(); ?>";
				$.ajax({
						type: 'POST',
						url: base_url+'admin/paper/deletePaperCategory',
						data: {id: id},
						success: function(data) {
							if(data == 1){
								$("#delCate"+del).empty();
							}
						}
				}); 
		} 
	}
	
	function edit_category(id)
	{
		$.ajax({
			url : "<?php echo base_url('admin/paper/category_edit')?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
				$('[name="category"]').val(data.category_name);
				$('[name="status"]').val(data.status);
				$('[name="categoryid"]').val(data.paper_category_id);
				$('#myModal').modal('show'); // show bootstrap modal when complete loaded
				$('.modal-title').text('Edit paper category'); // Set title to Bootstrap modal title */
	 
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		});
	}
</script> 