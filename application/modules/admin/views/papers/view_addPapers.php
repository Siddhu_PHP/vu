<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="<?php echo base_url('services');?>">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Add Paper</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<?php echo $this->session->flashdata('msg'); ?>
		<?php echo validation_errors('<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		<!-- END PAGE HEADER-->
		
		<div class="row">
			<div class="col-md-10">
				<!-- BEGIN VALIDATION STATES-->
				<div class="portlet light portlet-fit portlet-form bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-green"></i>
							<span class="caption-subject font-green sbold uppercase">Add Paper</span>
						</div>
					</div>
					<div class="portlet-body">
						<!-- BEGIN FORM-->
						<form name="addservice" method="POST" action="<?php echo base_url();?>admin/paper/addPaper" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal" id="form_sample_1">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
								<div class="alert alert-success display-hide">
									<button class="close" data-close="alert"></button> Your form validation is successful! </div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Paper Name
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="" name="prptitle" id="papertitle">
										<div class="form-control-focus"> </div>
										<span class="help-block">enter Paper title</span>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Paper URL
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="" name="prpurl" id="paperurl">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Paper Category</label>
									<div class="col-md-9">
										<select class="form-control" name="papercategory">
											<option value="">Select category</option>
										   <?php foreach($PaperCategory as $data){ ?>
											<option value="<?php echo $data->paper_category_id; ?>"><?php echo $data->category_name; ?></option>
											<?php } ?>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<!--<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Add Description</label>
									<div class="col-md-9">
										<textarea class="form-control" name="prpdesc" rows="3"></textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>-->
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Status</label>
									<div class="col-md-9">
										<select class="form-control" name="status">
											<option value='1'>Active</option>
											<option value='2'>Inactive</option>
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Image</label>
									<div class="col-md-9">
										<input type="file" class="form-control" placeholder="" name="simage" onchange="document.getElementById('pre-image').src = window.URL.createObjectURL(this.files[0])" accept="image/*">
										<img id="pre-image" alt="select image" width="100" height="100" />
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</div>
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		   
		   
		</div>
	 </div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->