<!-- Content Wrapper. Contains page content -->

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Edit Language
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
		  <?php echo $this->session->flashdata('msg'); ?>
		   <?php echo validation_errors('<div class="alert alert-warning">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		  <?php //print_r($ServiceCategory); ?>
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Language</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" name="addscategory" method="POST" action="<?php echo base_url(); ?>admin/addlang/editLang/<?php echo $Lang[0]->language_id; ?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="service">Language Name</label>
                      <input type="text" name="language" class="form-control" id="service" value="<?php echo $Lang[0]->language_name; ?>" placeholder="Category">
                   </div>
				
					<div class="form-group">
                      <label>Language Status</label>
                      <select class="form-control" name="status">
                        <option value='1' <?php if($Lang[0]->status == 1){echo "selected";}?>>Active</option>
                        <option value='2' <?php if($Lang[0]->status == 2){echo "selected";}?>>Inactive</option>
                      </select>
                    </div>
					             

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
             </div><!--/.col (left) -->
			
			
         
          </div>   <!-- /.row -->
        </section><!-- /.content -->
		
		
		
