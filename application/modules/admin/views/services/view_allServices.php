<style>
.modal-backdrop {
z-index: 10049 !important; 
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
<ul class="page-breadcrumb">
<li>
	<a href="<?php echo base_url('services');?>">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<a href="#">Services</a>
	<i class="fa fa-circle"></i>
</li>
</ul>
<div class="page-toolbar">
<div class="btn-group pull-right">
	<button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
						
		<i class="fa fa-angle-down"></i>
	</button>
	<ul class="dropdown-menu pull-right" role="menu">
		<li>
			<a href="#">
				<i class="icon-bell"></i> Action
			</a>
		</li>
		<li>
			<a href="#">
				<i class="icon-shield"></i> Another action
			</a>
		</li>
		<li>
			<a href="#">
				<i class="icon-user"></i> Something else here
			</a>
		</li>
		<li class="divider"></li>
		<li>
			<a href="#">
				<i class="icon-bag"></i> Separated link
			</a>
		</li>
	</ul>
</div>
</div>
</div>
 <div class="clearfix"> </div><br/>
<!-- END PAGE BAR -->
<!-- END PAGE HEADER-->
<div class="row">
<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-dark">
			<i class="icon-settings font-dark"></i>
			<span class="caption-subject bold uppercase"> Service Category List</span>
		</div>
		<div class="actions">
			<div class="btn-group btn-group-devided" data-toggle="buttons">
				<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
					<input type="radio" name="options" class="toggle" id="option1">Actions
					</label>
					<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option2">Settings
						</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group">
								<button id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" data-target="#myModal"> Add New Category
											
									<i class="fa fa-plus"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
					<thead>
						<tr>
							<th>
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
									<span></span>
								</label>
							</th>
							<th>S.no</th>
							<th>Service Category</th>
							<th>Created on</th>
							<th>Updated on</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; if(count($ServiceCategory) > 0){ ?>
						<?php foreach($ServiceCategory as $data){ ?>
						<tr class="odd gradeX"  id="delCate<?php echo $i; ?>">
							<td>
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="checkboxes" value="1" />
									<span></span>
								</label>
							</td>
							<td>
								<?php echo $i; ?>
							</td>
							<td>
								<?php echo $data->category_name; ?>
							</td>
							<td>
								<?php echo get_date($data->created_on,'d'); ?>
							</td>
							<td>
								<?php echo get_date($data->updated_on,'d'); ?>
							</td>
							<td>
								<?php echo getStatus ($data->status); ?>
							</td>
							<td>
								<a href="javascript:edit_category('<?php echo $data->service_category_id;?>');" ><i class="fa fa-edit"></i></a> &nbsp;&nbsp;&nbsp;
								<a href="javascript:delCate('<?php echo $data->service_category_id; ?>','<?php echo $i; ?>');"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php $i++;} ?>
						<?php }else{ ?>
						<tr>
							<td>Data not available</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-settings font-dark"></i>
					<span class="caption-subject bold uppercase"> Services List </span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
							<input type="radio" name="options" class="toggle" id="option1">Actions
							</label>
							<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
								<input type="radio" name="options" class="toggle" id="option2">Settings
								</label>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-12">
									<div class="btn-group">
										<a href="
											<?php echo base_url('admin/service/addservice/'); ?>">
											<button id="sample_editable_1_new" class="btn sbold green"> Add New Service										
												<i class="fa fa-plus"></i>
											</button>
										</a>
									</div>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
							<thead>
								<tr>
									<th>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
											<span></span>
										</label>
									</th>
									<th>S.no</th>
									<th>Service title</th>
									<th>Url</th>
									<th>Category</th>
									<th>Status</th>
									<th>Created on</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1; if(count($Service) > 0){ ?>
								<?php foreach($Service as $data){ ?>
								<tr class="odd gradeX"  id="del<?php echo $i; ?>">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>
										<?php echo $i; ?>
									</td>
									<td>
										<?php echo $data->service_title; ?>
									</td>
									<td>
										<?php echo $data->service_url; ?>
									</td>
									<td>
										<?php echo $data->category; ?>
									</td>
									<td>
										<?php  echo getStatus ($data->service_status); ?>
									</td>
									<td>
										<?php echo get_date($data->created_on,'d'); ?>
									</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
												
												<i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-left" role="menu">
												<li>
													<a href="
														<?php echo base_url('admin');?>/service/view_service/
														<?php echo $data->service_id; ?>">
														<i class="fa fa-eye"></i> View 
													</a>
												</li>
												<li>
													<a href="
														<?php echo base_url();?>admin/service/editService/
														<?php echo $data->service_id; ?>">
														<i class="fa fa-pencil-square-o"></i> Edit 
													</a>
												</li>
												<li>
					<a href="javascript:;" onClick="javascript: delthisid('<?php echo $data->service_id; ?>','<?php echo $i; ?>')">
														<i class="fa fa-trash-o"></i> Delete 
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
								<?php $i++;} ?>
								<?php }else{ ?>
								<tr>
									<td>Data not available</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div id="alert-msg"></div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add service category</h4>
			</div>
			<form action="" name="" method="post">
				<div class="modal-body">
					<div class="form-group row">
						<label for="example-text-input" class="col-xs-2 col-form-label">Name</label>
						<div class="col-xs-10">
							<input class="form-control" type="text"  name="category" id="category">
							<input type="hidden"  name="categoryid" id="categoryid">
							</div>
						</div>
						<div class="form-group row">
							<label for="example-search-input" class="col-xs-2 col-form-label">Status</label>
							<div class="col-xs-10">
								<select class="form-control" name="status" id="status">
									<option value ="1">Active</option>
									<option value ="0">Inactive</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="submit" class="btn green">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	