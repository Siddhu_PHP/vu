<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="<?php echo base_url('services');?>">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>View Service</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<?php echo $this->session->flashdata('msg'); ?>
		<?php echo validation_errors('<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		<!-- END PAGE HEADER-->
		
		<div class="row">
		<div class="col-md-10">
                                                            <div class="portlet yellow-crusta box">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-cogs"></i>Service Details </div>
                                                                    <div class="actions">
                                                                        <a href="<?php echo base_url('admin/service/editService/'.$Service[0]->service_id);?>" class="btn btn-default btn-sm">
                                                                            <i class="fa fa-pencil"></i> Edit </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Service Name: </div>
                                                                        <div class="col-md-7 value"> <?php echo $Service[0]->service_title; ?></div>
                                                                    </div>
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Service url: </div>
                                                                        <div class="col-md-7 value"> <?php echo $Service[0]->service_url; ?> </div>
                                                                    </div>
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Service Category: </div>
                                                                        <div class="col-md-7 value">
                                                                          <?php foreach($ServiceCategory as $data){ ?>
                       <?php if($data->service_category_id == $Service[0]->service_category_id) { echo $data->category_name;}?>
                        <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Description: </div>
                                                                        <div class="col-md-7 value"> <?php echo $Service[0]->service_description; ?> </div>
                                                                    </div>
																	<div class="row static-info">
                                                                        <div class="col-md-5 name"> Image: </div>
                                                                        <div class="col-md-7 value"> <img src="<?php echo base_url(); ?>uploads/service/<?php echo  $Service[0]->service_logo ?>" class="img-rounded" alt="Service img" width="204px" height="auto">  </div>
                                                                    </div>
																	<div class="row static-info">
                                                                        <div class="col-md-5 name"> Created on: </div>
                                                                        <div class="col-md-7 value"> <?php echo get_date($Service[0]->created_on,'d'); ?> </div>
                                                                    </div>
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Updated on: </div>
                                                                        <div class="col-md-7 value"> <?php echo get_date($Service[0]->updated_on, 'd'); ?> </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
		   
		   
		</div>
	 </div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->