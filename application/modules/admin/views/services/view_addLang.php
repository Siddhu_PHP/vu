<!-- Content Wrapper. Contains page content -->

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Add Language 
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
		  <?php echo $this->session->flashdata('msg'); ?>
		   <?php echo validation_errors('<div class="alert alert-warning">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		  <?php //print_r($ServiceCategory); ?>
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Language</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" name="addscategory" method="POST" action="<?php echo base_url();?>admin/addlang/index">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="Language">Language Name</label>
                      <input type="text" name="language" class="form-control" id="Language" placeholder="Category">
                   </div>
				
					<div class="form-group">
                      <label>Language Status</label>
                      <select class="form-control" name="status">
                        <option value='1'>Active</option>
                        <option value='2'>Inactive</option>
                      </select>
                    </div>
					             

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
             </div><!--/.col (left) -->
			
			
         
          </div>   <!-- /.row -->
        </section><!-- /.content -->
		
		
		<!--Data table Starts here-->
		  <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
            
                <div class="box-body">
               <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>S.no</th>
                <th>Language</th>
                <th>Created on</th>
                <th>Updated on</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>S.no</th>
                <th>Language</th>
                <th>Created on</th>
                <th>Updated on</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
		
		<?php $i=1; if(count($Lang) > 0){ ?>
		   <?php foreach($Lang as $data){ ?>
            <tr id="delete<?php echo $i; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $data->language_name; ?></td>
                <td><?php echo $data->created_on; ?></td>
                <td><?php echo $data->updated_on; ?></td>
                <td><?php echo $data->status; ?></td>
                <td><a href="<?php echo base_url();?>admin/addlang/editlang/<?php echo $data->language_id; ?>"><button><i class="fa fa-pencil-square-o"></i></button></a>&nbsp;<button onClick="javascript: delthisid('<?php echo $data->language_id; ?>','<?php echo $i; ?>')"><i class="fa fa-trash-o"></i></button></td>
            </tr>
			<?php $i++; } ?>
          <?php } else{ ?>
		    <tr>
                <td>No Data Available</td>	
            </tr>
		  <?php } ?>
        </tbody>
    </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

 
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
		<!-- Data table ends here -->
		
   
	  
	  
	<script type="text/javascript">
	function delthisid(id,del){ 
	
		 var r = confirm("Are you sure to delete this Language ?");
		if (r == true) {
			var base_url = "<?php echo base_url(); ?>";
				$.ajax({
					type: 'POST',
					url: base_url+'admin/addlang/deleteLang',
					data: {id: id},
					success: function(data) {
						if(data == 1){
						 	$("#delete"+del).empty();
						}
					

					}
				}); 
		} 
				
	  
	}
	</script> 