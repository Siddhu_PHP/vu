<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="<?php echo base_url('services');?>">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>View Location</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<?php echo $this->session->flashdata('msg'); ?>
		<?php echo validation_errors('<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		<!-- END PAGE HEADER-->
		
		<div class="row">
		<div class="col-md-10">
                                                            <div class="portlet yellow-crusta box">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-cogs"></i>Location Details </div>
                                                                    <div class="actions">
                                                                        <a href="<?php echo base_url('admin/Locations/editLocation/'.$id);?>" class="btn btn-default btn-sm">
                                                                            <i class="fa fa-pencil"></i> Edit </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Name: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['name']; ?></div>
                                                                    </div>
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Description: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['description']; ?> </div>
                                                                    </div>
                                                                   
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name">  Telugu Description: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['telugu']; ?> </div>
                                                                    </div>
																	
																	<div class="row static-info">
                                                                        <div class="col-md-5 name">  Other Information: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['info']; ?> </div>
                                                                    </div>
																	
																	<div id="map" style="width:100%;height:300px;"></div>
																		<div id="infowindow-content">
																		  <img src="" width="16" height="16" id="place-icon">
																		  <span id="place-name"  class="title"></span><br>
																		  <span id="place-address"></span>
																		</div>
																	
																	<div class="row static-info">
                                                                        <div class="col-md-5 name"> Address: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['address']; ?> </div>
                                                                    </div>
																	
																	<div class="row static-info">
                                                                        <div class="col-md-5 name">  video: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['video']; ?> </div>
                                                                    </div>
																	
																																																		<div class="row static-info">
                                                                        <div class="col-md-5 name">  Meta Description: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['meta_desc']; ?> </div>
                                                                    </div>
																	
																	<div class="row static-info">
                                                                        <div class="col-md-5 name"> Meta Keywords: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['meta_keywords']; ?> </div>
                                                                    </div>
																	
																	<?php if(count($location_images) > 0){
																		foreach($location_images as $images){
																	?>
																	
																	<div class="row static-info">
                                                                        <div class="col-md-5 name"> Image: </div>
                                                                        <div class="col-md-7 value"> <img alt="<?php echo $images["image_alt"]; ?>" width="100" height="100" src="<?php echo base_url("uploads/locations")."/".$images["name"]; ?>" />  </div>
                                                                    </div>
																	
																	<?php } } ?>
																	
																	<div class="row static-info">
                                                                        <div class="col-md-5 name"> Created on: </div>
                                                                        <div class="col-md-7 value"> <?php echo get_date($location['created_on'],'d'); ?> </div>
                                                                    </div>
                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Updated on: </div>
                                                                        <div class="col-md-7 value"> <?php echo get_date($location['updated_on'], 'd'); ?> </div>
                                                                    </div>                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Created by: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['created_by']; ?> </div>
                                                                    </div>                                                                    <div class="row static-info">
                                                                        <div class="col-md-5 name"> Updated by: </div>
                                                                        <div class="col-md-7 value"> <?php echo $location['updated_by']; ?> </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
		   
		   
		</div>
	 </div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->