<script>
$(document).ready(function() {
    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="clearfix"></div><div class="form-group form-md-line-input input_fields_wrap"><label class="col-md-3 control-label" for="form_control_1">Image</label><div class="col-md-9"><input type="file" class="form-control" placeholder="" name="simage[]" onchange="document.getElementById(\'pre-image'+x+'\').src = window.URL.createObjectURL(this.files[0])" accept="image/*"><img id="pre-image'+x+'" alt="select image" width="100" height="100" />  <button type="button" class="remove_field">-</button> <input type="text" class="form-control" placeholder="enter image alt name" name="alt[]" id=""><input type="checkbox" class="" value="'+x+'" name="defult_image[]"> Defult Image<div class="form-control-focus"> </div></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    })
	
	
});

$(document).on('click', 'input[type="checkbox"]', function() {      
    $('input[type="checkbox"]').not(this).prop('checked', false);      
});

var FormValidationMd = function() {
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation
        var form1 = $('#form_sample_1');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                payment: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                },
                'checkboxes1[]': {
                    required: 'Please check some options',
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                },
                'checkboxes2[]': {
                    required: 'Please check some options',
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                }
            },
            rules: {
                srctitle: {
                    minlength: 2,
                    required: true
                },
                srcurl: {
                    required: true
				},
                servicecategory: {
                    required: true
                },
				serdesc: {
                    required: true
                },
				simage: {
                    required: true
                }
                
            },

            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function(error, element) {
                if (element.is(':checkbox')) {
                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                } else if (element.is(':radio')) {
                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function(form) {
                success1.show();
                error1.hide();
				return true;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleValidation1();
        }
    };
}();

jQuery(document).ready(function() {
    FormValidationMd.init();
});
</script>



 
<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
	<script>
		CKEDITOR.replace( 'desc' );
		CKEDITOR.replace( 'telugu' );
		CKEDITOR.replace( 'info' );
	</script>
	
	
	<script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
		
		
		<?php if(isset($location['lat']) && !empty($location['lat']) && isset($location['longitude']) && !empty($location['longitude'])){?>
		  var lat = <?php echo $location['lat']; ?>; 
		  var longitude = <?php echo $location['longitude']; ?>; 
		  var address = "<?php echo $location['address']; ?>"; 
		  var myCenter = new google.maps.LatLng(lat,longitude);
		  var mapCanvas = document.getElementById("map");
		  var mapOptions = {center: myCenter, zoom: 5};
		  var map = new google.maps.Map(mapCanvas, mapOptions);
		   var marker = new google.maps.Marker({
			position: myCenter,
			animation: google.maps.Animation.BOUNCE
		  });
		  marker.setMap(map);
		  var infowindow = new google.maps.InfoWindow({
			content: address
		  });
		  infowindow.open(map,marker);
		<?php } ?>

       
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('address'));

        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
		  draggable: true,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
		 // console.log(place);
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
		  
		  
		  
		  	var address = place.formatted_address;
				//document.getElementById("formatted_address").value = address;
				
				var name = place.name;
				//document.getElementById("name").value = name;								
                
                var url = place.url;
				//document.getElementById("url").value = url;				
              
                var latitude = place.geometry.location.lat();
				
				document.getElementById("latitude").value = latitude;	
				
                var longitude = place.geometry.location.lng();
				document.getElementById("longitude").value = longitude;	

				var website = place.website;
				//document.getElementById("website").value = website;  

				$( place.address_components ).each(function( index, value ) {
					//console.log( index, value );
					
					 $( value.types ).each(function( key, mainvalue ) {
						//console.log( key, mainvalue );
							if(mainvalue === 'locality')
							{
								//console.log( "siddhu", value.long_name );
								document.getElementById("client_location").value = value.long_name;  
							}
						}); 
				});
		  
        });
		
		marker.addListener('dragend', function() {  
		  // console.log(this.getPosition().lat());
		  // console.log(this.getPosition().lng()); 
		  
		  $('input[name=latitude]').val(this.getPosition().lat());
		  $('input[name=longitude]').val(this.getPosition().lng());

			var geocoder = new google.maps.Geocoder;
		   // var infowindow = new google.maps.InfoWindow;
			  var latlng = {lat: parseFloat(this.getPosition().lat()), lng: parseFloat(this.getPosition().lng())};
			geocoder.geocode({'location': latlng}, function(results, status) {
			  if (status === 'OK') {
						//console.log(results);
						$( results ).each(function( index, value ) {
								console.log( index, value );
								
								 $( value.types ).each(function( key, mainvalue ) {
									//console.log( key, mainvalue );
										if(mainvalue === 'street_address' || mainvalue === 'route')
										{
											//alert(value.formatted_address);
											//alert(results[1].formatted_address);
											$('input[name=address]').val(results[1].formatted_address);								
											//$('input[name=test]').val(results[1].formatted_address);								
										}					
										
									}); 
							});
				
					if (results[1]) {
					  //map.setZoom(11);
					  /*var marker = new google.maps.Marker({
						position: latlng,
						map: map
					  });*/
					  infowindow.close();            
					  infowindow.setContent(results[1].formatted_address);
					  infowindow.open(map, marker);
					}
					else
					{
					  window.alert('No results found');
					}
			  }
			  else
			  {
				window.alert('Geocoder failed due to: ' + status);
			  }
			});		   
		});
  }
  
  
   </script>
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXkP7Z7L54odTOQJshVFVvQSt-FkAd77I&libraries=places&callback=initMap"
        async defer></script>
		
	<script>
		function deleteImage(id)
		{
			var result = confirm("Want to delete?");
			if (result) {
				var form_data = {
					id:id 				
				};
				$.ajax({
					url: "<?php echo base_url('admin/locations/delete_image'); ?>",
					type: 'POST',
					data: form_data,
					success: function(msg) {
						if (msg == 1){
							$('.remove_'+id).remove();
								
								
							}else{
							alert("unable to delete");
						}
					}
				});
			}
			
		}
	</script>	
	
	
	