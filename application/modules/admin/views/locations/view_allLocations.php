<style>
.modal-backdrop {
z-index: 10049 !important; 
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
<ul class="page-breadcrumb">
<li>
	<a href="<?php echo base_url('locations');?>">Home</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	<a href="#">locations</a>
	<i class="fa fa-circle"></i>
</li>
</ul>
<div class="page-toolbar">
<div class="btn-group pull-right">
	<button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
						
		<i class="fa fa-angle-down"></i>
	</button>
	<ul class="dropdown-menu pull-right" role="menu">
		<li>
			<a href="#">
				<i class="icon-bell"></i> Action
			</a>
		</li>
		<li>
			<a href="<?php echo base_url('admin/locations/showroworder'); ?>">
				<i class="icon-bell"></i> row order
			</a>
		</li>
		<li>
			<a href="#">
				<i class="icon-shield"></i> Another action
			</a>
		</li>
		<li>
			<a href="#">
				<i class="icon-user"></i> Something else here
			</a>
		</li>
		<li class="divider"></li>
		<li>
			<a href="#">
				<i class="icon-bag"></i> Separated link
			</a>
		</li>
	</ul>
</div>
</div>
</div>
 <div class="clearfix"> </div><br/>
<!-- END PAGE BAR -->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-settings font-dark"></i>
					<span class="caption-subject bold uppercase"> Locations List </span>
				</div>
			</div>
			<?php echo $this->session->flashdata('msg'); ?>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-12">
									<div class="btn-group">
										<a href="
											<?php echo base_url('admin/locations/addLocation/'); ?>">
											<button id="sample_editable_1_new" class="btn sbold green"> Add New Location										
												<i class="fa fa-plus"></i>
											</button>
										</a>
									</div>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
							<thead>
								<tr>
									<th>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
											<span></span>
										</label>
									</th>
									<th>S.no</th>
									<th>Location</th>
									<th>Info</th>
									<th>row order</th>
									<th>Status</th>
									<th>Created on</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1; if(count($locations) > 0){ ?>
								<?php foreach($locations as $data){ ?>
								<tr class="odd gradeX"  id="del<?php echo $i; ?>">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>
										<?php echo $i; ?>
									</td>
									<td>
										<?php echo $data['name']; ?>
									</td>
									<td>
										<?php echo $data['info']; ?>
									</td>
									<td>
										<?php echo $data['row_order']; ?>
									</td>
									<td>
										<?php  echo getStatus ($data['status']); ?>
									</td>
									<td>
										<?php echo get_date($data['created_on'],'d'); ?>
									</td>
									<td>
										<div class="btn-group">
											<button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
												
												<i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-left" role="menu">
												<li>
													<a href="
														<?php echo base_url('admin');?>/locations/view_location/
														<?php echo $data['location_id']; ?>">
														<i class="fa fa-eye"></i> View 
													</a>
												</li>
												<li>
													<a href="
														<?php echo base_url();?>admin/locations/editLocation/
														<?php echo $data['location_id']; ?>">
														<i class="fa fa-pencil-square-o"></i> Edit 
													</a>
												</li>
												<li>
					<a href="javascript:;" onClick="javascript: delthisid('<?php echo $data['location_id']; ?>','<?php echo $i; ?>')">
														<i class="fa fa-trash-o"></i> Delete 
													</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
								<?php $i++;} ?>
								<?php }else{ ?>
								<tr>
									<td>Data not available</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
