<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }
</style>


<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="<?php echo base_url('locations');?>">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Edit Location</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<?php echo $this->session->flashdata('msg'); ?>
		<?php echo validation_errors('<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', ' </div>'); ?>
		<!-- END PAGE HEADER-->
		
		<div class="row">
			<div class="col-md-10">
				<!-- BEGIN VALIDATION STATES-->
				<div class="portlet light portlet-fit portlet-form bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-green"></i>
							<span class="caption-subject font-green sbold uppercase">Edit Location</span>
						</div>
					</div>
					<div class="portlet-body">
						<!-- BEGIN FORM-->
						<form name="addservice" method="POST" action="<?php echo base_url();?>admin/locations/editLocation/<?php echo $id;?>" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal" id="form_sample_1">
							<div class="form-body">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
								<div class="alert alert-success display-hide">
									<button class="close" data-close="alert"></button> Your form validation is successful! </div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Location Name
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="" name="name" id="name" value="<?php echo $location['name']; ?>">
										<div class="form-control-focus"> </div>
										<span class="help-block">Enter Location title</span>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1"> Description
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<textarea class="form-control" rows="6" name="desc" id="desc" data-error-container="#editor1_error"><?php echo $location['description']; ?></textarea>
										<div class="form-control-focus"> </div>
										<span class="help-block">Enter Location description</span>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1"> Telugu Description
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<textarea class="form-control" rows="6" name="telugu" id="telugu" data-error-container="#editor1_error"><?php echo $location['telugu']; ?></textarea>
										<div class="form-control-focus"> </div>
										<span class="help-block">Enter telugu description</span>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1"> Other Information
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<textarea class="form-control" rows="6" name="info" id="info" data-error-container="#editor1_error"><?php echo $location['info']; ?></textarea>
										<div class="form-control-focus"> </div>
										<span class="help-block">Enter other Info</span>
									</div>
								</div>
								
								<div id="map" style="width:100%;height:300px;"></div>
								 <div id="infowindow-content">
								  <img src="" width="16" height="16" id="place-icon">
								  <span id="place-name"  class="title"></span><br>
								  <span id="place-address"></span>
								</div>
							
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Address
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="" name="address" id="address" value="<?php echo $location['address']; ?>">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Video
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="" name="video" id="video" value="<?php echo $location['video']; ?>">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Status</label>
									<div class="col-md-9">
										<select class="form-control" name="status">
											<option value="1" <?php if($location['status'] == 1){echo "selected";}?>>Active</option>
											<option value="0" <?php if($location['status'] == 0){echo "selected";}?>>Inactive</option>											
										</select>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Meta Description</label>
									<div class="col-md-9">
										<textarea class="form-control" name="metadesc" rows="3"><?php echo $location['meta_desc']; ?></textarea>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								 
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="form_control_1">Meta Keywords
										<span class="required">*</span>
									</label>
									<div class="col-md-9">
										<input type="text" class="form-control" placeholder="" name="keywords" id="keywords" value="<?php echo $location['meta_keywords']; ?>">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								
								<?php if(count($location_images) > 0){
									foreach($location_images as $images){
								?>
								<div class="form-group form-md-line-input <?php echo "remove_".$images["location_image_id"]; ?>">
									<label class="col-md-3 control-label" for="form_control_1">Image</label>
									<div class="col-md-9">
										<img alt="<?php echo $images["image_alt"]; ?>" width="100" height="100" src="<?php echo base_url("uploads/locations")."/".$images["name"]; ?>" />
										<button type="button" class="" onclick="deleteImage('<?php echo $images["location_image_id"]; ?>');">Remove</button>
										<div class="form-control-focus"> </div>									
									</div>									
								</div>
								<?php } } ?>
								
								
								<div class="form-group form-md-line-input input_fields_wrap">
									<label class="col-md-3 control-label" for="form_control_1">Image</label>
									<div class="col-md-9">
										<input type="file" class="form-control" placeholder="" name="simage[]" onchange="document.getElementById('pre-image').src = window.URL.createObjectURL(this.files[0])" accept="image/*">
										<img id="pre-image" alt="select image" width="100" height="100" /> <button type="button" class="add_field_button">+</button>
										<input type="text" class="form-control" placeholder="enter image alt name" name="alt[]" id="">
										<div class="form-control-focus"> </div>									
									</div>									
								</div>
								

								
								
							</div>
							
							<input type="hidden" name="latitude" id="latitude">
							<input type="hidden" name="longitude" id="longitude">
							<input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
							
							
							
							
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</div>
							
							
						</form>
						<!-- END FORM-->
					</div>
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		   
		   
		</div>
	 </div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->