<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('admin_model','admin');
		$this->load->model('mails_model','mails');
		
    }
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function validuser()
	{
		 $email = $this->input->post('email');
		 $password = $this->input->post('password'); 
		 
		 $this->form_validation->set_rules('email', 'Username', 'required');
         $this->form_validation->set_rules('password', 'Password', 'required');
		 
		  if ($this->form_validation->run($this) == FALSE)
                {
                    $this->load->view('login');
                }
                else
                {
                  $user = $this->admin->validate($email,$password);
				 
		
					if(count($user) > 0)
					{
						
						$data = array(
							'userId' => $user->user_id,
							'email' => $user->email,
							'usertype' => $user->user_role,
							'is_logged_in' => true
						);
						$this->session->set_userdata($data);
						redirect('admin/dashboard'); 
					}
                }	
	}
	
	public function dashboard()
	{
		$this->settemplate->admin('dashboard');
	}
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect('admin');       
		}       
	}
	
	
	function logout(){
		$this->session->sess_destroy();
		redirect('');
	}
	
	
	
	
	
	
	/* function _is_admin(){
		if(@$this->users->userdata()->role === 1){
			return true;
		}else{
			return false;
		}
	} */
	
			
	
	
	
	
	
	
}
