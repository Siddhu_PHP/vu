<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Addlang extends MX_Controller {

	 public function __construct()
		{
			parent::__construct();
			modules::run('admin/is_logged_in');
			$this->load->model('admin/service_model');
			
		}
 
	
    
	public function index()
		{	
			$data['Lang'] = $this->service_model->getLanguage();
			$this->form_validation->set_rules('language', 'language Category', 'required|trim|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean|numeric');
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('admin_common/header');
					$this->load->view('admin_common/sidebar');
					$this->load->view('services/view_addLang',$data);
					$this->load->view('admin_common/footer');
				}
				else
				{
					
					 $data = array(
					   'language_name' => $this->input->post('language') ,
					   'status' => $this->input->post('status') 
						);
					$result = $this->service_model->insertLang($data);	
					if($result){
						$this->session->set_flashdata('msg', '<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
					
					}else{
						$this->session->set_flashdata('msg', '<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
				   
					}		 
					  redirect('/admin/addlang/', 'refresh');
				}
		}
	
	public function deleteLang()
	{
		 $id = $this->input->post('id');
		 if(is_numeric($id) && isset($id)){
		  $data = array("status" => 3);
		  echo $result = $this->service_model->delLangByid($id,$data);
		 }
	}

	
	public function editlang($id)
	{	
		$data['Lang'] = $this->service_model->getLangByid($id);
		$this->form_validation->set_rules('language', 'language Category', 'required|trim|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean|numeric');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin_common/header');
			$this->load->view('admin_common/sidebar');
			$this->load->view('services/view_editLang',$data);
			$this->load->view('admin_common/footer');
		}
		else
		{
		 	 $data = array(
			   'language_name' => $this->input->post('language') ,
			   'status' => $this->input->post('status'),
			   'updated_on' => date('Y-m-d H:i:s')
			   	);
			$result = $this->service_model->updateLang($data, $id);	
			if($result){
				$this->session->set_flashdata('msg', '<div class="alert alert-success">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully Updated</div>');
		    
			}else{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
		   
			}		 
			  redirect('/admin/addlang/', 'refresh');
		}
		
	}
	
	
}