<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paper extends MX_Controller {

	 public function __construct()
		{
			parent::__construct();
			//modules::run('admin/is_logged_in');
			$this->load->model('Paper_model');
			$this->load->model('Upload_model');
			$this->load->helper('security');
		}
 
	public function index()
		{
			$data['PaperCategory'] = $this->Paper_model->getPaperCategories();
			$data['Papers'] = $this->Paper_model->getPaper();
			$this->settemplate->admin('papers/view_allPapers',$data);
			$this->load->view('papers/js/view_allPapers_js');
		}
	
	public function addPaper()
		{
			$data['PaperCategory'] = $this->Paper_model->getPaperCategories();
			$this->form_validation->set_rules('prptitle', 'Paper Title', 'required|trim');
			$this->form_validation->set_rules('prpurl', 'Paper URL', 'required|trim');
			$this->form_validation->set_rules('papercategory', 'Paper Category', 'required|trim|numeric');
			//$this->form_validation->set_rules('prpdesc', 'Description', 'required|trim|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
				if (empty($_FILES['simage']['name']))
				{
					$this->form_validation->set_rules('simage', 'Image Upload', 'required|trim');
				}
		
				 if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
					{
						$this->settemplate->admin('papers/view_addPapers',$data);
						$this->load->view('papers/js/addpaperJs');
					}
					else
					{
							$y = $_FILES['simage']['name'];
							if ($y != '') {
								$file_upl_data = $this->Upload_model->upload_img('paper', 'simage'); // pass 2 parrameters 1) folder name 2) image name
								if ($file_upl_data['success'] == 1) {
									$image = $file_upl_data['file_name'];
								} else{
									$this->session->set_flashdata('msg', $file_upl_data['errors']);
									 redirect('admin/paper/addPaper');
								}                
							}

						
				 		 $data = array(
									'paper_title'					=> 		$this->input->post('prptitle'),
									'paper_url' 					=> 		$this->input->post('prpurl'),
									'paper_category_id' 			=> 		$this->input->post('papercategory'), 
									'paper_logo' 					=>		$image,
									'paper_status'					=>		$this->input->post('status') 
							); 
							
						$result = $this->Paper_model->insertPaper($data);	
						
						if($result){
								$this->session->set_flashdata('msg', '<div class="alert alert-success">
							 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
							}else{
								$this->session->set_flashdata('msg', '<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
							}		 
							  redirect('/admin/paper/');
					}
		}
	
	
	public function editPaper($id)
		{
			$data['Paper'] = $this->Paper_model->getPaperByid($id);
			$data['PaperCategory'] = $this->Paper_model->getPaperCategories();
			$this->form_validation->set_rules('prptitle', 'Paper Title', 'required|trim');
			$this->form_validation->set_rules('prpurl', 'Paper URL', 'required|trim');
			$this->form_validation->set_rules('papercategory', 'Paper Category', 'required|trim|numeric');
			//$this->form_validation->set_rules('serdesc', 'Description', 'required|trim|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
			
			 if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
				{
					$this->settemplate->admin('papers/view_editPapers',$data);
				
				}
				else
				{
					
					$y = $_FILES['simage']['name'];
						if ($y != '') {
								$file_upl_data = $this->Upload_model->upload_img('paper', 'simage');
								if ($file_upl_data['success'] == 1) {
										$image = $file_upl_data['file_name'];
									}
								 $filename = FCPATH.'uploads/paper/'.$data['Paper'][0]->paper_logo; //  old img path 
							
								if (file_exists($filename)) {
										unlink($filename); // Deleted  old img in folder 
									}  
						} else{
									$image = $this->input->post('oldsimage');
									}            

						
				 		 $data = array(
									'paper_title'					=> 		$this->input->post('prptitle'),
									'paper_url' 					=> 		$this->input->post('prpurl'),
									'paper_category_id' 			=> 		$this->input->post('papercategory'), 
									'paper_logo' 					=>		$image,
									'paper_status'					=>		$this->input->post('status'),
									'updated_on'					=>		 date('Y-m-d H:i:s')
							); 
						
						$result = $this->Paper_model->updatePaper($data,$id);	
						
					if($result){
							$this->session->set_flashdata('msg', '<div class="alert alert-success">
						 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated </div>');
						}else{
							$this->session->set_flashdata('msg', '<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
						}		 
						  redirect('/admin/paper/');
				}
		}
	
	
	public function view_paper($id)
	{
		$data['Paper'] = $this->Paper_model->getPaperByid($id);
		print_r($data['Paper']);
		$data['PaperCategory'] = $this->Paper_model->getPaperCategories();
		$this->settemplate->admin('papers/view_PapersByid',$data);
		
	}
    
	public function addPaperCategory()
	{	
		$this->form_validation->set_rules('category', 'Paper Category', 'required|trim|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|xss_clean|numeric');
			if ($this->form_validation->run() == FALSE)
			{
				echo validation_errors();								
			}
			else
			{
				$cid = $this->input->post('categoryid');
				if(isset($cid) && ! empty($cid))
				{
					$data['category_name'] = $this->input->post('category') ;
					$data['status'] = $this->input->post('status') ;
					$data['updated_on'] = date('Y-m-d H:i:s');
					$result = $this->Paper_model->updateCategory($data,$cid);
				}
				else
				{
					$data = array(
				   'category_name' => $this->input->post('category') ,
				   'status' => $this->input->post('status') 
					);
					$result = $this->Paper_model->insertCategory($data);	
				}	
					 if($result)
					{
						echo "YES";
					}
					else
					{
						echo "NO";
					} 
			}
	}
	
	public function category_edit($id)
    {
        $data =	$this->Paper_model->getPaperCategoriesByid($id);
			echo json_encode($data);
    }	
	public function deletePaperCategory()
	{
		 $id = $this->input->post('id');
		 if(is_numeric($id) && isset($id)){
		  $data = array("status" => 3);
		  echo $result = $this->Paper_model->delPaperCategoriesByid($id,$data);
		 }
	}

	public function deletePaper()
	{
		 $id = $this->input->post('id');
		 if(is_numeric($id) && isset($id))
		 {
			  $data = array("paper_status" => 3);
			  echo $result = $this->Paper_model->delPaperByid($id,$data);
		}
	}
}