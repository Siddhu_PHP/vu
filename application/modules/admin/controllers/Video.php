<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Video extends MX_Controller {

	 public function __construct()
		{
			parent::__construct();
			modules::run('admin/is_logged_in');
			$this->load->model('Upload_model');
			$this->load->model('Video_model');
			$this->load->helper('security');
		}
 
	public function index()
		{
			$data['VideoCategory'] = $this->Video_model->getVideoCategories();
			$data['Video'] = $this->Video_model->getVideo();
			$this->settemplate->admin('videos/view_allVideos',$data);
			$this->load->view('videos/js/view_allVideos_js');
		}
	
	public function addVideo()
		{
			$data['VideoCategory'] = $this->Video_model->getVideoCategories();
			$this->form_validation->set_rules('vidtitle', 'Video Title', 'required|trim');
			$this->form_validation->set_rules('vidurl', 'Video URL', 'required|trim');
			$this->form_validation->set_rules('videocategory', 'Video Category', 'required|trim|numeric');
			$this->form_validation->set_rules('viddesc', 'Description', 'required|trim');
			$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
				if (empty($_FILES['simage']['name']))
				{
					$this->form_validation->set_rules('simage', 'Image Upload', 'required|trim|xss_clean');
				}
		
				 if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
					{
					    $this->settemplate->admin('videos/view_addVideos',$data);
						$this->load->view('videos/js/addvideoJs');
					}
					else
					{
							$y = $_FILES['simage']['name'];
							if ($y != '') {
								$file_upl_data = $this->Upload_model->upload_img('video', 'simage'); // pass 2 parrameters 1) folder name 2) image name
								if ($file_upl_data['success'] == 1) {
									$image = $file_upl_data['file_name'];
								} else{
									$this->session->set_flashdata('msg', $file_upl_data['errors']);
									 redirect('admin/video/addVideo');
								}                
							}

						
					 	 $data = array(
									'video_title'					=> 		$this->input->post('vidtitle'),
									'video_url' 					=> 		$this->input->post('vidurl'),
									'video_category_id' 			=> 		$this->input->post('videocategory'), 
									'video_logo' 					=>		$image,
									'video_description' 			=> 		$this->input->post('viddesc'), 
									'video_status'					=>		$this->input->post('status') 
							); 
							
						$result = $this->Video_model->insertVideo($data);	
						
						if($result){
								$this->session->set_flashdata('msg', '<div class="alert alert-success">
							 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
							}else{
								$this->session->set_flashdata('msg', '<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
							}		 
							  redirect('/admin/video/');
					}
		}
	
	
	public function editVideo($id)
		{
			$data['Video'] = $this->Video_model->getVideoByid($id);
			$data['VideoCategory'] = $this->Video_model->getVideoCategories();
			$this->form_validation->set_rules('vidtitle', 'Video Title', 'required|trim');
			$this->form_validation->set_rules('vidurl', 'Video URL', 'required|trim');
			$this->form_validation->set_rules('vidcategory', 'Video Category', 'required|trim|numeric');
			$this->form_validation->set_rules('viddesc', 'Description', 'required|trim');
			$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
			
			 if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
				{
					$this->settemplate->admin('videos/view_editVideos',$data);
				
				}
				else
				{
					
					$y = $_FILES['simage']['name'];
						if ($y != '') {
								$file_upl_data = $this->Upload_model->upload_img('video', 'simage');
								if ($file_upl_data['success'] == 1) {
										$image = $file_upl_data['file_name'];
									}
								$filename = FCPATH.'uploads/video/'.$data['Video'][0]->video_logo; //  old img path
								if (file_exists($filename)) {
										unlink($filename); // Deleted  old img in folder 
									} 
						} else{
									$image = $this->input->post('oldsimage');
									}            

						
				 		 $data = array(
									'video_title'					=> 		$this->input->post('vidtitle'),
									'video_url' 					=> 		$this->input->post('vidurl'),
									'video_category_id' 			=> 		$this->input->post('vidcategory'), 
									'video_logo' 					=>		$image,
									'video_description' 			=> 		$this->input->post('viddesc'), 
									'video_status'					=>		$this->input->post('status'),
									'video_status'					=>		$this->input->post('status'),
									'updated_on'					=> 		date('Y-m-d H:i:s')
							); 
						
						$result = $this->Video_model->updateVideo($data,$id);	
						
					if($result){
							$this->session->set_flashdata('msg', '<div class="alert alert-success">
						 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated </div>');
						}else{
							$this->session->set_flashdata('msg', '<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
						}		 
						  redirect('/admin/video/');
				}
		}
	
	
	public function viewVideo($id)
	{
		$data['Video'] = $this->Video_model->getVideoByid($id);
		$data['VideoCategory'] = $this->Video_model->getVideoCategories();
		$this->settemplate->admin('videos/view_VideosByid',$data);
		
	}
    
	public function addVideoCategory()
	{	
		$this->form_validation->set_rules('category', 'Video Category', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE)
		{
			echo validation_errors();		
		}
		else
		{
			$cid = $this->input->post('categoryid');	
			if(isset($cid) && ! empty($cid))
			{
				$data['category_name'] = $this->input->post('category') ;
				$data['status'] = $this->input->post('status') ;
				$data['updated_on'] = date('Y-m-d H:i:s');
				$result = $this->Video_model->updateCategory($data,$cid);
			}
			else
			{
				$data = array(
			   'category_name' => $this->input->post('category') ,
			   'status' => $this->input->post('status') 
				);
				$result = $this->Video_model->insertCategory($data);	
			}	
				if($result)
				{
					echo "YES";
				}
				else
				{
					echo "NO";
				} 
		
		}
	}
	public function category_edit($id)
    {
        $data =	$this->Video_model->getVideoCategoriesByid($id);
			echo json_encode($data);
    }	
		
	
	public function deleteVideoCategory()
	{
		 $id = $this->input->post('id');
		 if(is_numeric($id) && isset($id)){
		  $data = array("status" => 3);
		  echo $result = $this->Video_model->delVideoCategoriesByid($id,$data);
		 }
	}

	public function deleteVideo()
	{
		 $id = $this->input->post('id');
		 if(is_numeric($id) && isset($id)){
		  $data = array("video_status" => 3);
		  echo $result = $this->Video_model->delVideoByid($id,$data);
			}
	}
}