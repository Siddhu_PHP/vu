<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Locations extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		//modules::run('admin/is_logged_in');
		$this->load->model('Service_model');
		$this->load->model('location_model','location');
		$this->load->model('Upload_model');	
    }
	
	public function index()
	{
	    $data['locations'] = $this->location->getALL();
	    $this->settemplate->admin('locations/view_allLocations',$data);
		$this->load->view('locations/js/view_allLocations_js');
	}	
	

    public function showroworder()
    {
        $data['row_order_data'] = $this->location->getroworder();
        $this->settemplate->admin('locations/view_roworderLocation',$data);

        // print_r($data['row_order_data']);
        // exit;

    }

    public function update_roworder()
    {
		if(isset($_POST["submit"])) 
		{
		    $id_ary = explode(",",$_POST["row_order"]);
			for($i=0;$i<count($id_ary);$i++) 
			{
				$row_order = $i; 
				$id =  $id_ary[$i];
				
				$data = array(   
				'row_order' =>  $row_order
				);

                

				$res = $this->location->update_roworder($id,$data);
				if($res)
				{
				  redirect("index.php/admin/Locations");
				}

			}
		}

    }


	public function addLocation()
	{
		$this->form_validation->set_rules('name', 'Location Name', 'required|trim');
		$this->form_validation->set_rules('desc', 'Enter Location description', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
		if (empty($_FILES['simage']['name']))
		{
			$this->form_validation->set_rules('simage', 'Image Upload', 'required|trim|xss_clean');
		}
		if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
		{
			$this->settemplate->admin('locations/view_addLocation');
			$this->load->view('locations/js/addLocationJs');
		}
		else
		{
			$y = $_FILES['simage']['name'];
			if ($y != '')
			{
				$file_upl_data = $this->Upload_model->multiple_upload('locations', $y, 'simage'); // pass 2 parrameters 1) folder name 2) image name 3) filed name
				/*  print_r($file_upl_data); 
				print_r($file_upl_data['errors']); exit;  */
				if ($file_upl_data['success'] == 1)
				{
					
					$images = $file_upl_data;  // return array
				}
				else
				{
					
					$this->session->set_flashdata('msg', $file_upl_data['errors']);
					redirect('admin/locations/addLocation');
				}                
			}
								
			$data = array(
				'name'			=>	$this->input->post('name'),
				'description' 	=>	$this->input->post('desc'), 
				'info'			=>	$this->input->post('info'), 
				'lat'			=>	$this->input->post('latitude'), 
				'longitude'		=>	$this->input->post('longitude'), 
				'address'		=>	$this->input->post('address'),
				'video'			=>	$this->input->post('video'), 
				'row_order'		=>	0, 
				'status'		=>	$this->input->post('status'), 
				'url_slug'		=>	str_replace(" ","_",trim($this->input->post('name'))),
				'telugu'		=>  $this->input->post('telugu'),
				'meta_desc'		=>	$this->input->post('metadesc'), 
				'meta_keywords'	=>	$this->input->post('keywords'), 
				'created_by'	=>	1, //change later
				'created_on'	=>	date('Y-m-d H:i:s')
			);
			
			$result = $this->location->save($data);	
			$image_alt = $this->input->post('alt');
			$defult_image = $this->input->post('defult_image');
			//insert multiple images
			unset($images['success']);			
			if(count($images) > 0)
			{
				for($i=0;$i<count($images);$i++)
				{
					
					if(isset($images[$i]['file_name']) && !empty(($images[$i]['file_name'])))
					{
						echo $defult_image[$i].'-'.($i+1);
						$insertImages = array(
							'location_id' => $result,
							'name' => $images[$i]['file_name'],
							'image_alt' => $image_alt[$i]?$image_alt[$i]:"Vizagupdates.com",
							'defult_image' => ($defult_image[0] == ($i+1))?1:0,
						);
					}
					$insertImgData = $this->location->saveImg($insertImages);
				}
				
			}
			
			
			if($result)
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
			}
			else
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
			}		 
				redirect('/admin/locations');
		}
	}
		
	

	public function delete_image()
	{
		$id = $this->input->post('id');
		if(is_numeric($id) && isset($id))
		{
			$location_images = $this->location->getONEImage(array("location_image_id"=>$id));
			$filename = FCPATH.'uploads/locations/'.$location_images['name']; //  old img path
				if (file_exists($filename))
				{
					unlink($filename); // Deleted  old img in folder 
				} 
			
			echo $result = $this->location->deleteImage(array("location_image_id"=>$id));
		}
	}
	
	public function deleteServiceCategory()
	{
		$id = $this->input->post('id');
		if(is_numeric($id) && isset($id))
		{
			$data = array("status" => 3);
			echo $result = $this->Service_model->delServiceCategoriesByid($id,$data);
		}
	}
		
	public function editLocation($id)
	{
		
		$data['location'] = $this->location->getONE(array("location_id"=>$id));
		$data['id'] = $id;
		$data['location_images'] = $this->location->getALLImages(array("location_id"=>$id));
		$this->form_validation->set_rules('name', 'Location Name', 'required|trim');
		$this->form_validation->set_rules('desc', 'Enter Location description', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');

		if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
		{
			$this->settemplate->admin('locations/view_editLocaton',$data);
			$this->load->view('locations/js/addLocationJs');
		}
		else
		{
			           


			$y = $_FILES['simage']['name'];
			if ($y != '')
			{
				$file_upl_data = $this->Upload_model->multiple_upload('locations', $y, 'simage'); // pass 2 parrameters 1) folder name 2) image name 3) filed name
				/*  print_r($file_upl_data); 
				print_r($file_upl_data['errors']); exit;  */
				if ($file_upl_data['success'] == 1)
				{
					$images = $file_upl_data;  // return array
				}
				else if($file_upl_data['success'] == "empty")
				{
					$images = array();
				}
				else
				{
					$id = $this->input->post('id');	
					$this->session->set_flashdata('msg', $file_upl_data['errors']);
					redirect('admin/locations/editLocation/'.$id);
				}                
			}
			$id = $this->input->post('id');				
			$data = array(
				'name'			=>	$this->input->post('name'),
				'description' 	=>	$this->input->post('desc'), 
				'info'			=>	$this->input->post('info'), 
				'lat'			=>	$this->input->post('latitude'), 
				'longitude'		=>	$this->input->post('longitude'), 
				'address'		=>	$this->input->post('address'),
				'video'			=>	$this->input->post('video'), 
				'row_order'		=>	0, 
				'status'		=>	$this->input->post('status'), 
				'url_slug'		=>	str_replace(" ","_",trim($this->input->post('name'))),
				'telugu'		=>  $this->input->post('telugu'),
				'meta_desc'		=>	$this->input->post('metadesc'), 
				'meta_keywords'	=>	$this->input->post('keywords'), 
				'updated_by'	=>	1, //change later
				'updated_on'	=>	date('Y-m-d H:i:s')
			);
			
			$result = $this->location->save($data,array("location_id"=>$id));	
			$image_alt = $this->input->post('alt');
			
			//insert multiple images
			unset($images['success']);			
			if(count($images) > 0)
			{
				for($i=0;$i<count($images);$i++)
				{
					
					if(isset($images[$i]['file_name']) && !empty(($images[$i]['file_name'])))
					{
						$insertImages = array(
							'location_id' => $id,
							'name' => $images[$i]['file_name'],
							'image_alt' => $image_alt[$i],
						);
					}
					$insertImgData = $this->location->saveImg($insertImages);
				}
				
			}	

			if($result)
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated </div>');
			}
			else
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
			}		 
			redirect('/admin/locations/');
		}
	}
	
	
	public function view_location($id)
	{
		$data['location'] = $this->location->getONE(array("location_id"=>$id));
		$data['id'] = $id;
		$data['location_images'] = $this->location->getALLImages(array("location_id"=>$id));
		$this->settemplate->admin('locations/view_LocationByid',$data); 
		$this->load->view('locations/js/addLocationJs');
	}
	
	public function deleteLocation()
	{
		$id = $this->input->post('id');
		if(is_numeric($id) && isset($id))
		{
			$location_images = $this->location->getALLImages(array("location_id"=>$id));
			foreach($location_images as $location_image){
			$filename = FCPATH.'uploads/locations/'.$location_image['name']; //  old img path
				if (file_exists($filename))
				{
					unlink($filename); // Deleted  old img in folder 
				}
			}
			
			 $this->location->deleteImage(array("location_id"=>$id));
			echo $result = $this->location->deleteLocation(array("location_id"=>$id));
		}
	}
    
}
