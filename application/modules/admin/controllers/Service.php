<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Service extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		//modules::run('admin/is_logged_in');
		$this->load->model('Service_model');
		$this->load->model('Upload_model');	
    }
	
	public function index()
	{
	    $data['ServiceCategory'] = $this->Service_model->getServiceCategories();
		$data['Service'] = $this->Service_model->getService();
		$this->settemplate->admin('services/view_allServices',$data);
		$this->load->view('services/js/view_allServices_js');
	}	
	
	public function addService()
	{
		$data['ServiceCategory'] = $this->Service_model->getServiceCategories();
		$this->form_validation->set_rules('srctitle', 'Service Title', 'required|trim');
		$this->form_validation->set_rules('srcurl', 'Service URL', 'required|trim');
		$this->form_validation->set_rules('servicecategory', 'Service Category', 'required|trim|numeric');
		$this->form_validation->set_rules('serdesc', 'Description', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
		if (empty($_FILES['simage']['name']))
		{
			$this->form_validation->set_rules('simage', 'Image Upload', 'required|trim|xss_clean');
		}
		if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
		{
			$this->settemplate->admin('services/view_addServices',$data);
			$this->load->view('services/js/addserviceJs');
		}
		else
		{
			$y = $_FILES['simage']['name'];
			if ($y != '')
			{
				$file_upl_data = $this->Upload_model->upload_img('service', 'simage'); // pass 2 parrameters 1) folder name 2) image name
				if ($file_upl_data['success'] == 1)
				{
					$image = $file_upl_data['file_name'];
				}
				else
				{
					$this->session->set_flashdata('msg', $file_upl_data['errors']);
					redirect('admin/service/addService');
				}                
			}					
			$data = array(
				'service_title'			=>	$this->input->post('srctitle'),
				'service_url' 			=>	$this->input->post('srcurl'),
				'service_category_id'	=>	$this->input->post('servicecategory'), 
				'service_logo'			=>	$image,
				'service_description'	=>	$this->input->post('serdesc'), 
				'service_status'		=>	$this->input->post('status'),
				'updated_on'					=>		 date('Y-m-d H:i:s')
			);
			$result = $this->Service_model->insertService($data);	
			if($result)
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Success </div>');
			}
			else
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
			}		 
				redirect('/admin/service/');
		}
	}
		
	public function addServiceCategory()
	{	
		$this->form_validation->set_rules('category', 'Service Category', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE)
		{
			echo validation_errors();				
		}
		else
		{
			$cid = $this->input->post('categoryid');	
			if(isset($cid) && ! empty($cid))
			{
				$data['category_name'] = $this->input->post('category') ;
				$data['status'] = $this->input->post('status') ;
				$data['updated_on'] = date('Y-m-d H:i:s');
				$result = $this->Service_model->updateCategory($data,$cid);
			}
			else
			{
				$data = array(
			   'category_name' => $this->input->post('category') ,
			   'status' => $this->input->post('status') 
				);
				$result = $this->Service_model->insertCategory($data);	
			}	
				if($result)
				{
					echo "YES";
				}
				else
				{
					echo "NO";
				} 
		}
	}
	
	public function category_edit($id)
    {
        $data =	$this->Service_model->getServiceCategoriesByid($id);
			echo json_encode($data);
    }	

	public function deleteService()
	{
		$id = $this->input->post('id');
		if(is_numeric($id) && isset($id))
		{
			$data = array("service_status" => 3);
			echo $result = $this->Service_model->delServiceByid($id,$data);
		}
	}
	public function deleteServiceCategory()
	{
		$id = $this->input->post('id');
		if(is_numeric($id) && isset($id))
		{
			$data = array("status" => 3);
			echo $result = $this->Service_model->delServiceCategoriesByid($id,$data);
		}
	}
		
	public function editService($id)
	{
		$data['Service'] = $this->Service_model->getServiceByid($id);
		$data['ServiceCategory'] = $this->Service_model->getServiceCategories();
		$this->form_validation->set_rules('srctitle', 'Service Title', 'required|trim');
		$this->form_validation->set_rules('srcurl', 'Service URL', 'required|trim');
		$this->form_validation->set_rules('servicecategory', 'Service Category', 'required|trim|numeric');
		$this->form_validation->set_rules('serdesc', 'Description', 'required|trim');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|numeric');

		if ($this->form_validation->run($this) === FALSE) // use $this HMVC FRAMEWORK
		{
			$this->settemplate->admin('services/view_editServices',$data);
		}
		else
		{
			$y = $_FILES['simage']['name'];
			if ($y != '')
			{
				$file_upl_data = $this->Upload_model->upload_img('service', 'simage');
				if ($file_upl_data['success'] == 1)
				{
					$image = $file_upl_data['file_name'];
				}
				$filename = FCPATH.'uploads/service/'.$data['Service'][0]->service_logo; //  old img path
				if (file_exists($filename))
				{
					unlink($filename); // Deleted  old img in folder 
				} 
			}
			else
			{
				$image = $this->input->post('oldsimage');
			}            


			$data = array(
			'service_title'	=>	$this->input->post('srctitle'),
			'service_url'	=>	$this->input->post('srcurl'),
			'service_category_id'	=>	$this->input->post('servicecategory'), 
			'service_position'	=>	$this->input->post('position'), 
			'service_logo'	=>	$image,
			'service_description'	=>	$this->input->post('serdesc'), 
			'service_status'	=>	$this->input->post('status') 
			);

			$result = $this->Service_model->updateService($data,$id);	

			if($result)
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully updated </div>');
			}
			else
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Error </div> ');
			}		 
			redirect('/admin/service/');
		}
	}
	
	
	public function view_service($id)
	{
		$data['Service'] = $this->Service_model->getServiceByid($id);
		$data['ServiceCategory'] = $this->Service_model->getServiceCategories();
		$this->settemplate->admin('services/view_ServicesByid',$data); 
	}
    
}
