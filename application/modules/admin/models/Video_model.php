<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Video_model extends CI_Model  {

private $videocategory = 'tbl_video_categories'; 
private $video = 'tbl_video_channels'; 
  public function __construct()
  {
    parent::__construct();
  }
	public function insertCategory($data)
	{
	   $this->db->insert($this->videocategory, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	public function updateCategory($data, $id)
	{
	   $this->db->where('video_category_id', $id);
	   $this->db->update($this->videocategory, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	public function updateVideo($data, $id)
	{
	   $this->db->where('video_id', $id);
	   $this->db->update($this->video, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}

	public function getVideoCategories()
	{
		$result = $this->db->query("SELECT video_category_id, category_name, created_on, updated_on, status FROM {$this->videocategory} WHERE status != 3 ORDER BY video_category_id DESC")->result();
		return $result;
	} 
	public function getVideoCategoriesByid($id)
	{
		if (is_numeric($id)) {
        $result = $this->db->query("SELECT video_category_id, category_name, created_on, updated_on, status FROM {$this->videocategory} WHERE status != 3 AND video_category_id = {$id}")->row();
		} else {
		$result = array();
		}
		return $result;
	} 
	public function delVideoCategoriesByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('video_category_id', $id);
	    $this->db->update($this->videocategory, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function delVideoByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('video_id', $id);
	    $this->db->update($this->video, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function insertVideo($data)
	{
	   $this->db->insert($this->video, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}

	public function getVideo()
	{
		$result = $this->db->query("SELECT s.video_id, s.video_title, s.video_url,  s.video_position,  s.created_on, s.video_status, (SELECT cs.category_name FROM {$this->videocategory} cs WHERE cs.video_category_id =  s.video_category_id AND cs.status != 3 ) AS category FROM {$this->video} s WHERE s.video_status != 3 ORDER BY s.video_id DESC")->result();
		return $result;
	}

	public function getVideoByid($id)
	{
		$result = $this->db->query("SELECT s.video_id, s.video_title, s.video_url,  s.video_position,  s.created_on,  s.updated_on, s.video_logo,  s.video_description, s.video_status, s.video_category_id, (SELECT cs.category_name FROM {$this->videocategory} cs WHERE cs.video_category_id =  s.video_category_id AND cs.status != 3 ) AS category FROM {$this->video} s WHERE s.video_status != 3 ORDER BY s.video_id DESC")->result();
		return $result;
	}
}