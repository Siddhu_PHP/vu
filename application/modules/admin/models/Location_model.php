<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Location_model extends CI_Model  {

	private $locations = 'vu_locations'; 
	private $location_images = 'vu_location_images'; 

	public function __construct()
	{
		parent::__construct();
	}
	
	public function getALL($param = "")
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$this->db->where($param);			
		}
		$this->db->order_by('created_on', 'DESC');
		$locations = $this->db->get($this->locations);
		if($locations->num_rows() > 0)
		{
			foreach($locations->result_array() as $location)
			{
				$array[] = $location;
			}
		}
		return $array;
	}
	

	public function getroworder()
	{
		$this->db->select('location_id,name,row_order');
		$this->db->from($this->locations);
		$query = $this->db->get();	 
		if($query->num_rows()>0)
		{
		  return ($query->result_array());
		}
	}
	
    public function update_roworder($id,$data)
    {
		$this->db->where("location_id",$id);
		$this->db->update($this->locations,$data); 
		if($this->db->affected_rows() > 0)
		{
		  return TRUE;
		}

    }



	public function getONE($param)
	{
		$array = array();
		if(is_array($param) && count($param)>0)
		{
			$customers = $this->db->limit(1)->get_where($this->locations,$param);
			if($customers->num_rows() > 0)
			{
				return $customers->row_array();
			}
		}
		return $array;
	}
	
	public function save($data,$con="")
	{
		
		if(is_array($con))
		{
			return	$this->db->update($this->locations,$data,$con);			/*update in to database*/
		}
		else
		{
			$this->db->insert($this->locations,$data);	
			 return $this->db->insert_id();			/*Inserting in to database*/	
		}			
		
	}
	
	public function saveImg($data,$con="")
	{
		
		if(is_array($con))
		{
			return	$this->db->update($this->location_images,$data,$con);			/*update in to database*/
		}
		else
		{
			$this->db->insert($this->location_images,$data);	
			 return $this->db->insert_id();			/*Inserting in to database*/	
		}			
		
	}
	
	public function getALLImages($param = "")
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$this->db->where($param);			
		}
		$locations = $this->db->get($this->location_images);
		if($locations->num_rows() > 0)
		{
			foreach($locations->result_array() as $location)
			{
				$array[] = $location;
			}
		}
		return $array;
	}
	
	public function getONEImage($param)
	{
		$array = array();
		if(is_array($param) && count($param)>0)
		{
			$customers = $this->db->limit(1)->get_where($this->location_images,$param);
			if($customers->num_rows() > 0)
			{
				return $customers->row_array();
			}
		}
		return $array;
	}
	
	public function deleteImage($param)
	{
		if(is_array($param) && count($param)>0)
		{
			return $this->db->delete($this->location_images, $param); 
		}
	}
	
	public function deleteLocation($param)
	{
		if(is_array($param) && count($param)>0)
		{
			$this->db->delete($this->location_images, $param); 
			return $this->db->delete($this->locations, $param); 
		}
	}
	


	
	
	
}
?>