<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class paper_model extends CI_Model  {

private $papercategory = 'tbl_paper_categories'; 
private $paper = 'tbl_paper'; 
  public function __construct()
  {
    parent::__construct();
  }
	public function insertCategory($data)
	{
	   $this->db->insert($this->papercategory, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	public function updateCategory($data, $id)
	{
	   $this->db->where('paper_category_id', $id);
	   $this->db->update($this->papercategory, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	public function updatepaper($data, $id)
	{
	   $this->db->where('paper_id', $id);
	   $this->db->update($this->paper, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}

	public function getpaperCategories()
	{
		$result = $this->db->query("SELECT paper_category_id, category_name, created_on, updated_on, status FROM {$this->papercategory} WHERE status != 3 ORDER BY paper_category_id DESC")->result();
		return $result;
	} 
	public function getpaperCategoriesByid($id)
	{
		if (is_numeric($id)) {
        $result = $this->db->query("SELECT paper_category_id, category_name, created_on, updated_on, status FROM {$this->papercategory} WHERE status != 3 AND paper_category_id = {$id}")->row();
		} else {
		$result = array();
		}
		return $result;
	} 
	public function delpaperCategoriesByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('paper_category_id', $id);
	    $this->db->update($this->papercategory, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function delpaperByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('paper_id', $id);
	    $this->db->update($this->paper, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function insertpaper($data)
	{
	   $this->db->insert($this->paper, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}

	public function getpaper()
	{
		$result = $this->db->query("SELECT s.paper_id, s.paper_title, s.paper_url,  s.paper_position,  s.created_on, s.paper_status, (SELECT cs.category_name FROM {$this->papercategory} cs WHERE cs.paper_category_id =  s.paper_category_id AND cs.status != 3 ) AS category FROM {$this->paper} s WHERE s.paper_status != 3 ORDER BY s.paper_id DESC")->result();
		return $result;
	}

	public function getpaperByid($id)
	{
		$result = $this->db->query("SELECT s.paper_id, s.paper_title, s.paper_url,  s.paper_position,  s.created_on,  s.updated_on, s.paper_logo, s.paper_status, s.paper_category_id, (SELECT cs.category_name FROM {$this->papercategory} cs WHERE cs.paper_category_id =  s.paper_category_id AND cs.status != 3 ) AS category FROM {$this->paper} s WHERE s.paper_status != 3 ORDER BY s.paper_id DESC")->result();
		return $result;
	}
}