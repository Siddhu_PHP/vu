<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Service_model extends CI_Model  {

private $servicecategory = 'tbl_service_categories'; 
private $service = 'tbl_services'; 
private $lang = 'tbl_languages'; 
  public function __construct()
  {
    parent::__construct();
  }
	public function insertCategory($data)
	{
	   $this->db->insert($this->servicecategory, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	public function updateCategory($data, $id)
	{
	   $this->db->where('service_category_id', $id);
	   $this->db->update($this->servicecategory, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	
	public function updateService($data, $id)
	{
	   $this->db->where('service_id', $id);
	   $this->db->update($this->service, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}

	public function getServiceCategories()
	{
		$result = $this->db->query("SELECT service_category_id, category_name, created_on, updated_on, status FROM {$this->servicecategory} WHERE status != 3 ORDER BY service_category_id DESC")->result();
		return $result;
	} 
	public function getServiceCategoriesByid($id)
	{
		if (is_numeric($id))
		{
			$result = $this->db->query("SELECT service_category_id, category_name, created_on, updated_on, status FROM {$this->servicecategory} WHERE status != 3 AND service_category_id = {$id}")->row();
		}
		else
		{
			$result = array();
		}
		return $result;
	}
	
	public function delServiceCategoriesByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('service_category_id', $id);
	    $this->db->update($this->servicecategory, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	}
	
	
	public function delServiceByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('service_id', $id);
	    $this->db->update($this->service, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function insertService($data)
	{
	   $this->db->insert($this->service, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	

	public function getService()
	{
		$result = $this->db->query("SELECT s.service_id, s.service_title, s.service_url,  s.service_position,  s.created_on, s.service_status, (SELECT cs.category_name FROM {$this->servicecategory} cs WHERE cs.service_category_id =  s.service_category_id AND cs.status != 3 ) AS category FROM {$this->service} s WHERE s.service_status != 3 ORDER BY s.service_id DESC")->result();
		return $result;
	}

	public function getServiceByid($id)
	{
		$result = $this->db->query("SELECT s.service_id, s.service_title, s.service_url,  s.service_position,  s.created_on,  s.updated_on, s.service_logo,  s.service_description, s.service_status, s.service_category_id, (SELECT cs.category_name FROM {$this->servicecategory} cs WHERE cs.service_category_id =  s.service_category_id AND cs.status != 3 ) AS category FROM {$this->service} s WHERE s.service_status != 3 ORDER BY s.service_id DESC")->result();
		return $result;
	}
	
	public function getLanguage()
	{
		$result = $this->db->query("SELECT * FROM {$this->lang} WHERE status != 3 ORDER BY language_id DESC")->result();
		return $result;
	}
	
	public function insertLang($data)
	{
	   $this->db->insert($this->lang, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
	
	public function delLangByid($id,$data)
	{
		if (is_numeric($id)) {
        $this->db->where('language_id', $id);
	    $this->db->update($this->lang, $data);
			if($this->db->affected_rows() > 0) {
				$result = 1;
			}
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function getLangByid($id)
	{
		if (is_numeric($id)) {
        $result = $this->db->query("SELECT language_id, language_name, created_on, updated_on, status FROM {$this->lang} WHERE status != 3 AND language_id = {$id}")->result();
		} else {
		$result = array();
		}
		return $result;
	} 
	
	public function updateLang($data, $id)
	{
	   $this->db->where('language_id', $id);
	   $this->db->update($this->lang, $data);
		if($this->db->affected_rows() > 0) {
			return true;
		}
	}
}
?>