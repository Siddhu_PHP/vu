 <?php if(isset($_GET['msg']) && ($_GET['msg'] == 'Activated')){ ?>
  <script src="<?php echo base_url('assets/frontend/sweet_alert'); ?>/dist/sweetalert.js"></script>
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend/sweet_alert'); ?>/dist/sweetalert.css">
  <script>
	  $( document ).ready(function() {
        swal("Activated!", "Your account successfully activated!", "success");
      });
	
    </script>
 <?php } ?>