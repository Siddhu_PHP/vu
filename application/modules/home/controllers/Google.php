<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('users/user_model','user');
		$this->load->model('Upload_model');	
    }
	
	public function index()
	{
		
		if($this->session->userdata('is_logged_in') == true){
			redirect('');
		} 
		
		if (isset($_GET['code'])) {
			
			$this->googleplus->getAuthenticate();
			$user = $this->googleplus->getUserInfo();
			
			if($user['picture'] != '' && isset($user['picture']))
			{			
				//insert profile picture into database and folder
				$content = file_get_contents($user['picture']);
				//Store in the filesystem.
				$picName = date('ymdHis').'profile.jpg';
				$fp = fopen("./uploads/profilepicture/$picName", "w");
				fwrite($fp, $content);
				fclose($fp);
				$this->Upload_model->_create_thumbnail($picName,50,40);
			}
			else
			{
				$picName = '';
			}
			
			$data = array(
				'name' => $user['name']?$user['name']:'',
				'email' => $user['email'],
				'gender' => $user['gender']?$user['gender']:'',
				'picture' => $picName,
				'user_role' => 2,
				'created_on' => date('Y-m-d H:i:s'),
				'status' => 1,
				'ip_address' =>  $this->input->ip_address(),
				'activation_key' => 'vizagupdates-active'.date('ymdHis').rand(0,1000),
				'register_by' => 'google',
			);
			
			$isExists = $this->user->mail_exists($user['email']);
			if($isExists == true){
			$insert_id = $this->user->insertUser($data);
			$data = ['user_id' => $insert_id];
			$result = $this->user->getONE($data);
			}else{
			$data = ['email' => $user['email']];
			$result = $this->user->getONE($data);
			}
			if($result){
			$data = array(
					'userId' => $result['user_id'],
					'email' => $result['email'],
					'usertype' => $result['user_role'],
					'picture' => $result['picture'],
					'is_logged_in' => true
					);
			$this->session->set_userdata($data);
			}else{
				show_404();
			}
			redirect($this->session->userdata('url'));
			
		} 
			
		// if not login. login URL will create here	
		  $contents['login_url'] = $this->googleplus->loginURL();
		  $contents['login_url'];
		  redirect($contents['login_url']);
		
	}
	
	
	
	public function logout(){
		
		$this->session->sess_destroy();
		$this->googleplus->revokeToken();
		redirect('');
		
	}
	
	
	
}
