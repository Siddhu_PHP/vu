<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		require_once(APPPATH . 'libraries/Facebook/autoload.php');
		$this->load->model('users/user_model','user');
		$this->load->model('Upload_model');
		
    }
	
	public function index()
	{
		$fb = new Facebook\Facebook([
					  'app_id' => '789507494527114', 
					  'app_secret' => 'bafb87ee6206b23f472de523bfde27c3',
					  'default_graph_version' => 'v2.4',
					  ]);

					$helper = $fb->getRedirectLoginHelper();

					$permissions = ['email','user_location','user_birthday','publish_actions','publish_pages','manage_pages','public_profile',]; // Optional permissions
					
  
					$loginUrl = $helper->getLoginUrl('http://vizagupdates.com/vu/home/facebook/test', $permissions);
                    
					redirect($loginUrl);
	}
	
		
	public function test()
	{
		$fb = new Facebook\Facebook([
					  'app_id' => '789507494527114', 
					  'app_secret' => 'bafb87ee6206b23f472de523bfde27c3',
					  'default_graph_version' => 'v2.4',
					  ]);

		$helper = $fb->getRedirectLoginHelper();

		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		if (! isset($accessToken)) {
		  if ($helper->getError()) {
			header('HTTP/1.0 401 Unauthorized');
			echo "Error: " . $helper->getError() . "\n";
			echo "Error Code: " . $helper->getErrorCode() . "\n";
			echo "Error Reason: " . $helper->getErrorReason() . "\n";
			echo "Error Description: " . $helper->getErrorDescription() . "\n";
		  } else {
			header('HTTP/1.0 400 Bad Request');
			echo 'Bad request';
		  }
		  exit;
		}

		// Logged in
		/* echo '<h3>Access Token</h3>'; */
		/* var_dump($accessToken->getValue()); siddhu */

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		/* echo '<h3>Metadata</h3>'; */
		/* var_dump($tokenMetadata); siddhu */

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId('789507494527114'); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived()) {
		  // Exchanges a short-lived access token for a long-lived one
		  try {
			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (Facebook\Exceptions\FacebookSDKException $e) {
			echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
			exit;
		  }

		  echo '<h3>Long-lived</h3>';
		  var_dump($accessToken->getValue());
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;
		$data = array(
				'fb_access_token' => (string) $accessToken,
				);
		$this->session->set_userdata($data);

		// User is logged in with a long-lived access token.
		// You can redirect them to a members-only page.
		//header('Location: https://example.com/members.php');
		
		
		try {
			  // Returns a `Facebook\FacebookResponse` object
			  $response = $fb->get('/me?fields=id,name,gender,email,picture.width(300)', $accessToken);
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
			  echo 'Graph returned an error: ' . $e->getMessage();
			  exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
			  echo 'Facebook SDK returned an error: ' . $e->getMessage();
			  exit;
			}

			$user = $response->getGraphUser();
			$json = json_decode($user['picture'], true);
			
			print_r($user);
			/* print_r($user['name']);
			print_r($json);
			exit; */
			if($json['url'] != '' && isset($json['url']))
			{
				//insert profile picture into database and folder
				$content = file_get_contents($json['url']);
				//Store in the filesystem.
				$picName = date('ymdHis').'profile.jpg';
				$fp = fopen("./uploads/profilepicture/$picName", "w");
				fwrite($fp, $content);
				fclose($fp);
				$this->Upload_model->_create_thumbnail($picName,50,40);
			}
			else
			{
				$picName = '';
			}
			$data = array(
				'name' => $user['name'],
				'email' => $user['email'],
				'gender' => $user['gender'],
				'picture' => $picName,
				'user_role' => 2,
				'created_on' => date('Y-m-d H:i:s'),
				'status' => 1,
				'ip_address' =>  $this->input->ip_address(),
				'activation_key' => 'vizagupdates-active'.date('ymdHis').rand(0,1000),
				'register_by' => 'facebook',
			);
			
			$isExists = $this->user->mail_exists($user['email']);
			if($isExists == true)
			{
				$insert_id = $this->user->insertUser($data);
				$data = ['user_id' => $insert_id];
				$result = $this->user->getONE($data);
			}
			else
			{
				$data = ['email' => $user['email']];
				$result = $this->user->getONE($data);
			}
			if($result){
			$data = array(
					'userId' => $result['user_id'],
					'email' => $result['email'],
					'usertype' => $result['user_role'],
					'picture' => $result['picture'],
					'is_logged_in' => true
					);
			$this->session->set_userdata($data);
			}else{
				show_404();
			}
			redirect($this->session->userdata('url'));
	}

	public function success()
	{
		
		$fb = new Facebook\Facebook([
					  'app_id' => '789507494527114', 
					  'app_secret' => 'bafb87ee6206b23f472de523bfde27c3',
					  'default_graph_version' => 'v2.4',
					  ]);
	
		$linkData = [
		  'link' => 'http://www.vizagupdates.com',
		  'message' => 'User provided message',
		  'picture' => 'http://www.vizagupdates.com/vu/assets/frontend/images/logo.png',
		  'description' => 'The description of the link (appears beneath the link caption). If not specified, this field is automatically populated by information scraped from the link, typically the title of the page.',
		  ];

		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $fb->post('/me/feed', $linkData, $this->session->userdata('fb_access_token'));
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		$graphNode = $response->getGraphNode();

		echo 'Posted with id: ' . $graphNode['id'];
			
	}
	
	
			
}
