<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Govt extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('govt_model','govt');
		
    }
	
	public function index()
	{
		$data['govt'] = $this->govt->getALL(['service_status' => 1]);
		$this->settemplate->home('service',$data);
	}
	
}
