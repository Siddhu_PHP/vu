
  <!-- Main Content Start -->
  
  <div class="main-content"> 
    
    <!-- Post Details -->
    <div class="cp-gallery">
      <div class="container">
        <div class="row">
          <div class="cp-masonry-posts">
            <div class="isotope items"> 
              
			  
			<?php if(count($govt) > 0){ ?>  
			  <?php foreach($govt as $data){ ?>
			   <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb"><img src="<?php echo base_url('uploads/service/'.$data['service_logo']); ?>" alt="govt services" width="360px" height="250px"></div>
                  <div class="cp-post-content">
                    <h3><a href="<?php echo $data['service_id']; ?>"><?php echo $data['service_title']; ?></a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p><?php echo $data['service_description']; ?></p>
                  </div>
                </div>
              </div>
			  <?php } ?>
              <!--CP Post End-->
  
			 <?php } else { ?>
			 
			  <div class="item">
                <div class="cp-post">
                 <h2>Data not available </h2>
                </div>
              </div>

			<?php } ?>
			  
      
            </div>
            
           <!-- <div class="pagination-holder">
                <nav>
                  <ul class="pagination">
                    <li> <a aria-label="Previous" href="#"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li> <a aria-label="Next" href="#"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                  </ul>
                </nav>
              </div> -->
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fashion Category  Main Post End --> 
  