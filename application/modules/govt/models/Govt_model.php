<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*------------------------------------------------
			news_model
--------------------------------------------------
Author		:-	Siddhu
Date		:-	1st July-2016
Data Table	:-	news
*/
class Govt_model extends CI_Model
{
	private $tableName;
	public function __construct() {
        parent::__construct();
		$this->tableName = 'tbl_services';	/*---Assigning table name-----*/	
    }
	
	/*------------------------------------------------------------
						getONE
	--------------------------------------------------------------
	@Author			:	Siddhu
	@Date			:	22-06-2016
	@Objective		:	To get the existed user details
	@Params			:	$param(array) contains 1 or more key and values
	@Returns		:	User data as an array		
	*/
	public function getONE($param)
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$user_data = $this->db->limit(1)->get_where($this->tableName,$param);
			if($user_data->num_rows() > 0)
			{
				return $user_data->row_array();
			}
		}
		return $array;
	}
	/*----------------getALL-----------------------
	*@Author	:	Siddhu
	*@Date		:	24-06-2016
	*@Objective	:	To get multiple users from database
	*@Params	:	$param(array) contains 0 or more key and values
	*@Return	:	Users(array)
	*/
	public function getALL($param = "")
	{
		$array = array();
		if(is_array($param) && count($param) > 0)
		{
			$this->db->where($param);			
		}
		$user_data = $this->db->get($this->tableName);
		if($user_data->num_rows() > 0)
		{
			foreach($user_data->result_array() as $user)
			{
				$array[] = $user;
			}
		}
		return $array;
	}
	
	/*----------------save-----------------------
	*@Author	:	Siddhu
	*@Date		:	23-06-2016
	*@Objective	:	To save user
	*@Params	:	userdata(array)
	*@Return	:	status
	*/
	public function save($data,$con="")
	{
		if(is_array($con))
		{
			$this->db->update($this->tableName,$data,$con);			/*Inserting in to database*/
		}
		else
		{
			$this->db->insert($this->tableName,$data);			/*Inserting in to database*/	
		}			
		
	}
	
	/*----------------Delete-----------------------
	*@Author	:	Siddhu
	*@Date		:	23-06-2016
	*@Objective	:	To save user
	*@Params	:	userdata(array)
	*@Return	:	status
	*/
	function row_delete($id)
	{
	   $this->db->where('id', $id);
	   $this->db->delete($this->tableName); 
	}
}
