  <!-- Main Content Start -->
  
  <div class="main-content"> 
    
    <!-- Post Details -->
    <div class="cp-gallery">
      <div class="container">
        <div class="row">
          <div class="cp-masonry-posts">
            <div class="isotope items"> 
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post sticky">
                  <div class="cp-thumb"><img src="http://html.crunchpress.com/materialmag/images/sports4.jpg" alt=""></div>
                  <div class="cp-post-content">
                    <h3><a href="#">This is my Sticky Post</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using lorem ipsum is that it has a more-or-less.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb"><img src="http://html.crunchpress.com/materialmag/images/sports4.jpg" alt=""></div>
                  <div class="cp-post-content">
                    <div class="cp-post-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb">
                    <audio preload="auto" controls>
                      <source src="audio/BlueDucks_FourFlossFiveSix.mp3">
                      <source src="audio/BlueDucks_FourFlossFiveSix.ogg">
                      <source src="audio/BlueDucks_FourFlossFiveSix.wav">
                    </audio>
                  </div>
                  <div class="cp-post-content">
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
    
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-post-content">
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Nelson Doe</li>
                      <li><i class="icon-3"></i> Quote</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <blockquote>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia fugit, voluptas sit aspernatur aut odit aut  sed quia.</blockquote>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb">
                    <iframe src="https://player.vimeo.com/video/126089225" style="width:100%; min-height:250px; border:0px;"></iframe>
                  </div>
                  <div class="cp-post-content">
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              
           
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb">
                    <iframe style="width:100%; height:auto; border:0px;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/92141893&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                  </div>
                  <div class="cp-post-content">
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using lorem ipsum is that it has a more-or-less.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              
            
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb"><img src="http://html.crunchpress.com/materialmag/images/sports4.jpg" alt=""></div>
                  <div class="cp-post-content">
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>We denounce with righteous indignation and dislike men who are so beguiled.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-thumb"><img src="images/fnew-07.jpg" alt=""></div>
                  <div class="cp-post-content">
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using lorem ipsum is that it has a more-or-less.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End-->
              
                          <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-post-content">
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Nelson Doe</li>
                      <li><i class="icon-3"></i> Quote</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <strong class="link-post">http://themes.crunchpress.com/?theme=material-mag</strong> </div>
                </div>
              </div>
              <!--CP Post End--> 
              
              
               <!--CP Post Start-->
              <div class="item">
                <div class="cp-post">
                  <div class="cp-post-content">
                    <h3><a href="#">Duis tristique tellus egestas est aliquam
                      nisl finibus vehicula it</a></h3>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-2"></i> Roy Miller</li>
                      <li><i class="icon-3"></i> Lifestyle</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using lorem ipsum is that it has a more-or-less.</p>
                  </div>
                </div>
              </div>
              <!--CP Post End-->
              
              
             
              
              
              
              
       
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
            </div>
            
            <div class="pagination-holder">
                <nav>
                  <ul class="pagination">
                    <li> <a aria-label="Previous" href="#"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li> <a aria-label="Next" href="#"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                  </ul>
                </nav>
              </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fashion Category  Main Post End --> 
  