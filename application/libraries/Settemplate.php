<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
class Settemplate 
{   
   var $CI ,$ID='',$TYPE;
   function __construct() {
         // Copy an instance of CI so we can use the entire framework.
      $this->CI = & get_instance();
	  $this->CI->load->library("template");
   }
   
   public function admin($view, $data="", $title="Code3")
  	{
		$this->CI->template->set_template("admin");
		
		$content = $this->CI->load->view($view, $data, true);
		$this->CI->template->write ('content',$content);
		
		$this->CI->template->write ('title',$title);
		
		$css = $this->CI->load->view("templates/backend/css",$data, true);
		$this->CI->template->write ('css',$css);
		
		$quick_slider = $this->CI->load->view("templates/backend/css",$data, true);
		$this->CI->template->write ('css',$quick_slider);
		
		
		$menu = $this->CI->load->view("templates/backend/menu",$data, true);
		$this->CI->template->write ('menu',$menu);
		
		$header = $this->CI->load->view("templates/backend/header",$data, true);
		$this->CI->template->write ('header',$header);
		
		$footer = $this->CI->load->view("templates/backend/footer",$data, true);
		$this->CI->template->write ('footer',$footer);
		
		$js = $this->CI->load->view("templates/backend/js",$data, true);
		$this->CI->template->write ('js',$js);
		
		
		$this->CI->template->render();
  	}
	
       
	public function home($view, $data="", $title="Vizag Updates")
  	{
		$this->CI->template->set_template("home");
		
		 $content = $this->CI->load->view($view, $data, true);
		$this->CI->template->write ('content',$content);
		
		/* $this->CI->template->write ('title',$title); */
		
		$css = $this->CI->load->view("templates/frontend/css",$data, true);
		$this->CI->template->write ('css',$css);
		
		$meta = $this->CI->load->view("templates/frontend/meta",$data, true);
		$this->CI->template->write ('meta',$meta);
		
		/* $menu = $this->CI->load->view("templates/backend/menu",$data, true);
		$this->CI->template->write ('menu',$menu); */
		
		$header = $this->CI->load->view("templates/frontend/header",$data, true);
		$this->CI->template->write ('header',$header);
		
		$footer = $this->CI->load->view("templates/frontend/footer",$data, true);
		$this->CI->template->write ('footer',$footer);
		 
		$js = $this->CI->load->view("templates/frontend/js",$data, true);
		$this->CI->template->write ('js',$js);
		
		
		$this->CI->template->render();
  	} 
	 
}
?>