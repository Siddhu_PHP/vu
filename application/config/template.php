<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$template['active_template']='admin';

//template 1
$template['admin']['template']='backend/admin';
$template['admin']['regions']=array(
  'css',
  'header',
  'title',
  'content',
  'quick_slider',
  'menu',
  'footer',
  'js',
);
$template['admin']['parser']='parser';
$template['admin']['parser_method']='parse';
$template['admin']['parse_template']=FALSE;

$template['home']['template']='frontend/home';
$template['home']['regions']=array(
  'css',
  'header',
  'meta',
  'content',
  'js',
  'footer'
  
);
$template['home']['parser']='parser';
$template['home']['parser_method']='parse';
$template['home']['parse_template']=FALSE;
?>