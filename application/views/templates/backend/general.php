<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
<!DOCTYPE html>
<html>
<?php echo $css;?>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
	<?php echo $header;?>
	<?php echo $menu;?>
	<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <?php echo $content;?>
    <!-- Main content -->
   
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php echo $footer;?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

</body>
<?php echo $js;?>
</html>
