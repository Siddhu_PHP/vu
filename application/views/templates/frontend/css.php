<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
<title>Vizag Updates</title>
<link href="<?php echo $asset_url;?>frontend/css/custom.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo $asset_url;?>frontend/css/custom_siddhu.css" rel="stylesheet">
<!-- Siddhu CSS -->
<link href="<?php echo $asset_url;?>frontend/css/color.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo $asset_url;?>frontend/css/mega-menu.css" rel="stylesheet">
<!-- Mega Menu -->
<link href="<?php echo $asset_url;?>frontend/css/bootstrap.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="<?php echo $asset_url;?>frontend/css/bootstrap-theme.min.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="<?php echo $asset_url;?>frontend/css/materialize.css" rel="stylesheet">
<!-- Materialize CSS -->
<link href="<?php echo $asset_url;?>frontend/css/font-awesome.min.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link href="<?php echo $asset_url;?>frontend/css/owl.slider.css" rel="stylesheet">
<!-- Owl Slider -->

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->