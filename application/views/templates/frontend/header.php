<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>

  <!-- Header Start -->
  <div id="cp-header" class="cp-header"> 
    
    <!-- Topbar Start -->
    <div class="cp-topbar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <ul class="toplinks">
              <li class="waves-effect waves-button"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="waves-effect waves-button"><a href="<?php echo base_url(); ?>">News</a></li>
              <li class="waves-effect waves-button"><a href="<?php echo base_url(); ?>">FAQ’s</a></li>
              <li class="waves-effect waves-button"><i class="fa fa-phone"></i> +91 9912238386</li>
              <li class="waves-effect waves-button"><i class="fa fa-envelope-o"></i> <a href="mailto:uvizag@gmail.com">uvizag@gmail.com</a></li>
            </ul>
          </div>
          <div class="col-md-6">
            <div class="cp-toptools pull-right">
              <ul>
			  <?php
			  if($this->session->userdata('is_logged_in'))
			  {
				$pic = $this->session->userdata('picture');
			?>
					
				<li class="waves-effect" style="background:none !important"><img src="<?php echo $pic; ?>" alt="" class="img-responsive img-circle"></li>
				<a href="<?php echo base_url('users/logout'); ?>"><li class="waves-effect"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout </li></a>
			<?php }else{ ?>
			<li class="" data-toggle="modal" data-target="#loginmodal"><i class="icon-2"> Login / Register</i></li>
			<?php } ?>
               
              
              </ul>
            </div>
            <div class="cp-topsocial pull-right">
              <ul>
                <li class="waves-effect"><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="waves-effect"><a target="_blank" href="https://www.facebook.com/vizag360/"><i class="fa fa-facebook"></i></a></li>
                <li class="waves-effect"><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></li>
                <li class="waves-effect"><a target="_blank" href="#"><i class="fa fa-youtube"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Topbar End --> 
    
    <!-- Logo row Start --> 
    <div class="cp-logo-row">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo"><a href="<?php echo base_url(); ?>"><img style="height:103px; margin-top: -30px;" src="<?php echo $asset_url;?>frontend/images/logo.svg" alt=""></a></div>
          </div>
          <div class="col-md-8">
            <div class="cp-advertisement waves-effect"><img src="<?php echo $asset_url;?>frontend/images/ad-large.gif" alt=""></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Logo row Start --> 
    
    <!-- Mega Menu Start -->
    <div class="cp-megamenu">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="cp-mega-menu">
              <label for="mobile-button"><i class="fa fa-bars"></i></label>
              <!-- mobile click button to show menu -->
              <input id="mobile-button" type="checkbox">
              <ul class="collapse main-menu">
              <li class="slogo"><a href="index.html"><img style="height:43px; width:51px;" src="<?php echo $asset_url;?>frontend/images/logo-micon.svg" alt=""></a></li>
                
                <li><a href="<?php echo base_url(); ?>">Home</a>
                 <!-- <ul class="drop-down one-column hover-expand">
                    <!-- first level drop down 
                    <li> <a href="index.html">Home Layout One</a> </li>
                    <li> <a href="index-layout-02.html">Home Layout Two</a> </li>
                    <li> <a href="index-layout-03.html">Home Layout Three</a> </li>
                  </ul> -->
                </li>
                <li> <a href="<?php echo base_url('govt');?>">Govt Services</a>
                  <!-- <ul class="drop-down full-width col-5 hover-expand">
                    <!-- full width drop down with 5 columns + images 
                    <li class="validation">
                      <h2 class="mm-title">Govt Services</h2>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-1.jpg" alt="">
                      <h3><a href="<?php echo base_url(); ?>">AP Government portals and services. Get information about all services</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-2.jpg" alt="">
                      <h3><a href="<?php echo base_url(); ?>">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-1.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-4.jpg" alt="">
                      <h3><a href="category-layout-1.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-5.jpg" alt="">
                      <h3><a href="category-layout-1.html">Nullam tincidunt lorem sit amet imperdiet sollicitu.</a></h3>
                    </li>
                  </ul> -->
                </li>
                <li> <a href="<?php echo base_url(); ?>">E - News papers</a>
                  <ul class="drop-down full-width col-5 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li class="validation">
                      <h2 class="mm-title">E - News papers</h2>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-1.jpg" alt="">
                      <h3><a href="category-layout-4.html">Get all online e-news papers </a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-2.jpg" alt="">
                      <h3><a href="category-layout-4.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-4.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-4.jpg" alt="">
                      <h3><a href="#">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-5.jpg" alt="">
                      <h3><a href="category-layout-4.html">Nullam tincidunt lorem sit amet imperdiet sollicitu.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="<?php echo base_url(); ?>">Channels</a>
                  <ul class="drop-down full-width col-4 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li>
                      <ul class="sub-menu">
                        <li> <a href="#">About</a> </li>
                        <li> <a href="#">Testimonials</a> </li>
                        <li> <a href="author-archives.html">Archives</a> </li>
                        <li> <a href="gallery-full.html">Gallery</a> </li>
                        <li> <a href="contact.html">Contact Page</a> </li>
                        <li> <a href="page-404.html">404 Page</a> </li>
                      </ul>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-2.jpg" alt="">
                      <h3><a href="<?php echo base_url(); ?>">Watch any live channels online. 
					  </a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-3.jpg" alt="">
                      <h3><a href="<?php echo base_url(); ?>">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-4.jpg" alt="">
                      <h3><a href="category-layout-2.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="<?php echo base_url(); ?>">Real estate</a>
                  <ul class="drop-down full-width col-5 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li class="validation">
                      <h2 class="mm-title">Real estate</h2>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-1.jpg" alt="">
                      <h3><a href="category-layout-3.html">Find best lands, plots and apartments in vizag, verified VUDA layouts </a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-2.jpg" alt="">
                      <h3><a href="category-layout-3.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-3.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-4.jpg" alt="">
                      <h3><a href="category-layout-3.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="<?php echo $asset_url;?>frontend/images/mm-5.jpg" alt="">
                      <h3><a href="category-layout-3.html">Nullam tincidunt lorem sit amet imperdiet sollicitu.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="<?php echo base_url(); ?>">Tourism</a>
                  <ul class="drop-down one-column hover-fade">
                    <!-- first level drop down -->
                    
                    <li> <a href="author-archives-sidebar.html">Post Format Archive</a> </li>
                    <li> <a href="#">Single Post</a> </li>
                    <li> <a href="#">WooCommerce Shop</a><i class="fa fa-angle-right"></i>
                      <ul class="drop-down one-column hover-expand">
                        <!-- second level drop down -->
                        <li> <a href="products.html">Shop</a> </li>
                        <li> <a href="product-details.html">Product Details</a> </li>
                        <li> <a href="login.html">Login</a> </li>
                        <li> <a href="register.html">Registers</a> </li>
                      </ul>
                    </li>
                    <li> <a href="authors.html">Author</a> </li>
                    <li> <a href="author-archives.html">Author Archive</a> </li>
                    <li> <a href="date-archives.html">Date Archive</a> </li>
                    <li> <a href="tags.html">Tag Archive</a> </li>
                    <li> <a href="search-results.html">Search Results</a> </li>
                    <li> <a href="contact.html">Contact Page</a> </li>
                    <li> <a href="page-404.html">404 Page</a> </li>
                    <li> <a href="#">Gallery</a> <i class="fa fa-angle-right"></i> <!-- fontAwesome icon -->
                      
                      <ul class="drop-down one-column hover-expand">
                        <!-- second level drop down -->
                        <li> <a href="gallery-classic.html">Gallery Classic</a> </li>
                        <li> <a href="gallery-elite.html">Gallery Elite View</a> </li>
                        <li> <a href="gallery-full.html">Gallery Full</a> </li>
                        <li> <a href="gallery-2col.html">Gallery Large</a> </li>
                        <li> <a href="gallery-masonry.html">Gallery Masonry</a> </li>
                        <li> <a href="gallery-medium.html">Gallery Medium</a> </li>
                        <li> <a href="gallery-small.html">Gallery Small</a> </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li> <a href="#">Blog</a>
                  <ul class="drop-down full-width blog-menu hover-expand">
                    <li>
                      <ul>
                        <li> <a href="#"> <img src="<?php echo $asset_url;?>frontend/images/mm-1.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                        <li> <a href="#"> <img src="<?php echo $asset_url;?>frontend/images/mm-2.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <li> <a href="#"> <img src="<?php echo $asset_url;?>frontend/images/mm-3.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                        <li> <a href="#"> <img src="<?php echo $asset_url;?>frontend/images/mm-4.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <!-- column three -->
                        <li class="validation">
                          <h2>Blog Layouts</h2>
                        </li>
                        <li> <a href="blog-full.html"> Blog Full</a> </li>
                        <li> <a href="blog-medium.html"> Blog Medium</a> </li>
                        <li> <a href="blog-column.html"> Blog Colum</a> </li>
                        <li> <a href="blog-grid-modern.html"> Blog Grid Modren</a> </li>
                        <li> <a href="blog-top-featured.html"> Blog Top Featured</a> </li>
                        <li> <a href="blog-masonry.html"> Blog Masonry</a> </li>
                        <li> <a href="single-post.html"> Single Post</a> </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <!-- column four -->
                        <li class="validation">
                          <h2>Blog Category</h2>
                        </li>
                        <li> <a href="category-layout-3.html">Photography</a> </li>
                        <li> <a href="category-layout-2.html">Sports</a> </li>
                        <li> <a href="category-layout-1.html">Fashion</a> </li>
                        <li> <a href="category-layout-4.html">Lifestyle</a> </li>
                        <li> <a href="#">World</a> </li>
                        <li> <a href="#">Health</a> </li>
                        <li> <a href="#">Technology</a> </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="author-archives-sidebar.html">Archive</a></li>
                <li> <a href="#">Shortcodes</a>
                  <div class="drop-down full-width text-links hover-expand"> <!-- full width drop down with 4 columns -->
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                  </div>
                </li>
                <li><a href="contact.html">Contact Us</a></li>
                <li class="search-bar"> <i class="icon-7"></i> <!-- search bar -->
                  
                  <ul class="drop-down hover-expand">
                    <li>
                      <form method="post" >
                        <table>
                          <tr>
                            <td><input type="text" name="serach_bar" placeholder="Type Keyword Here"></td>
                            <td><input type="submit" value="Search"></td>
                          </tr>
                        </table>
                      </form>
                    </li>
                  </ul>
                </li>
                <li class="random"><a href="random.html"><i class="icon-6"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Mega Menu End --> 
    
  </div>
  <!-- Header End --> 