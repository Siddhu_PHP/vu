<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
<!-- Js Files--> 
<script src="<?php echo $asset_url;?>frontend/js/jquery-1.11.2.min.js"></script> 
<script src="<?php echo $asset_url;?>frontend/js/jquery-migrate-1.2.1.min.js"></script> 
<script src="<?php echo $asset_url;?>frontend/js/bootstrap.min.js"></script> 
<script src="<?php echo $asset_url;?>frontend/js/materialize.min.js"></script> 
<script src="<?php echo $asset_url;?>frontend/js/owl.carousel.min.js"></script>
<script src="<?php echo $asset_url;?>frontend/js/custom.js"></script>
<!--pop up script starts-->
<script type="text/javascript">
$('#submit').click(function() {
	    var form_data = {
        email: $('#login_email').val(),
        password: $('#password').val(),
        redirect: $('#redirect').val()
        
    };
    $.ajax({
        url: "<?php echo site_url('users/validuser'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
            if (msg == '1')
			{
				window.location.href = "<?php echo $this->session->userdata('url'); ?>";
			}                 
            else if (msg == '2')
			{
				 $('#alert-msg').html('<div class="alert alert-danger">Invalid Login Details</div>');
			}               
            else
			{
				$('#alert-msg').html('<div class="alert alert-danger text-center">Error in sending your message! Please try again later.</div>');
			}
               
			 
        }
    });
    return false;
});

//register
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url = "<?php echo base_url('users/register')?>";
     $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
		/* dataType: "JSON", */
        success: function(data)
        {
		 if(data == "yes")
		 {
			 $(".after_succes").hide();
			 $(".success").show();
		 }
		 else
		 {
			var obj = jQuery.parseJSON( data );					 
			$('#error_name').html(obj.name);
			$('#error_email').html(obj.email);
			$('#error_password').html(obj.password);
			$('#error_cpassword').html(obj.cpassword);
			$('#btnSave').text('save'); //change button text
			$('#btnSave').attr('disabled',false); //set button enable
		 }
        },
			error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        }
    });
}


//forget
$('#forget').click(function() {
	var email = $('#forget_email').val(); 
	if(email == '')
	{
		$('#forget-msg').html('<div class="alert alert-danger text-center">Please enter the registered email.</div>');
	}
	var form_data = {
        email: email       
    };
    $.ajax({
        url: "<?php echo site_url('users/forgetpassword'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
			if (msg == 1){
				
				$('#forget-msg').html('<div class="alert alert-success text-center">Instructions to reset your password has been sent to your email. Please check your for these next steps</div>');
                 
            }else if (msg == 2){
                $('#forget-msg').html('<div class="alert alert-danger text-center">Email not exists in our database. Please register :-) </div>');
            }else{
                $('#forget-msg').html('<div class="alert alert-danger">Email not exists in our database. Please register :-)</div>');
			}
        }
    });
    return false;
});

</script>
<!--pop up script ends-->