<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?><!-- footer start -->
 <footer id="footer" class="footer">
    <div class="footer-one footer-widgets">
      <div class="container">
        <div class="row">
          <div class="col-md-3"> 
            
            <!-- Text Widget -->
            <div class="widget textwidget">
              <h3>Text Widget</h3>
              <div class="cp-widget-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat.</p>
              </div>
            </div>
            <!-- Text Widget End --> 
          </div>
          <div class="col-md-3"> 
            <!-- Fatured posts -->
            <div class="widget">
              <h3>Featured Posts</h3>
              <div class="cp-widget-content">
                <ul class="featured-posts">
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                  </li>
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 09, 2015</li>
                      <li><i class="icon-4"></i> 15 Comments</li>
                    </ul>
                  </li>
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 11, 2015</li>
                      <li><i class="icon-4"></i> 60 Comments</li>
                    </ul>
                  </li>
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 15, 2015</li>
                      <li><i class="icon-4"></i> 80 Comments</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- Fatured posts End --> 
          </div>
          <div class="col-md-3"> 
            <!-- Popular posts -->
            <div class="widget">
              <h3>Latest Reviews</h3>
              <div class="cp-widget-content">
                <ul class="reviews">
                  <li>
                    <h4><a href="#">Fusce quis nisi cursus</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">8.2</i> </li>
                  <li>
                    <h4><a href="#">Morbi vel metus vitae</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">9.2</i> </li>
                  <li>
                    <h4><a href="#">Proin ut sapien tempor</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">7.2</i> </li>
                  <li>
                    <h4><a href="#">Vivamus feugiat lacus</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">5.2</i> </li>
                </ul>
              </div>
            </div>
            <!-- Popular posts End --> 
          </div>
          <div class="col-md-3"> 
            <!-- Popular Posts -->
            <div class="widget popular-posts ">
              <h3>Popular</h3>
              <div class="cp-widget-content">
                <ul class="small-grid">
                  <li>
                    <div class="small-post">
                      <div class="cp-thumb"><img src="<?php echo $asset_url;?>frontend/images/pgthumb-6.jpg" alt=""></div>
                      <div class="cp-post-content">
                        <h4><a href="#">Quisque sit amet est</a></h4>
                        <ul class="cp-post-tools">
                          <li><i class="icon-1"></i> May 10, 2015</li>
                          <li><i class="icon-4"></i> 57 Comments</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="small-post">
                      <div class="cp-thumb"><img src="<?php echo $asset_url;?>frontend/images/pgthumb-1.jpg" alt=""></div>
                      <div class="cp-post-content">
                        <h4><a href="#">Quisque sit amet est</a></h4>
                        <ul class="cp-post-tools">
                          <li><i class="icon-1"></i> May 10, 2015</li>
                          <li><i class="icon-4"></i> 57 Comments</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="small-post">
                      <div class="cp-thumb"><img src="<?php echo $asset_url;?>frontend/images/pgthumb-2.jpg" alt=""></div>
                      <div class="cp-post-content">
                        <h4><a href="#">Quisque sit amet est</a></h4>
                        <ul class="cp-post-tools">
                          <li><i class="icon-1"></i> May 10, 2015</li>
                          <li><i class="icon-4"></i> 57 Comments</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            
            <!-- Popular Posts End --> 
            
          </div>
        </div>
      </div>
    </div>
    <div class="footer-two footer-widgets">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="widget form-widget">
              <h3>Stay connected</h3>
              <div class="cp-widget-content">
                <form  class="material">
                  <div class="input-group">
                    <input type="text" name="name" placeholder="Name" required>
                    <input type="email" name="email" placeholder="Email Address" required>
                    <input type="text" name="subject" placeholder="Subject">
                    <textarea name="message" placeholder="Message"></textarea>
                  </div>
                  <button class="btn btn-submit waves-effect waves-button" type="submit">Submit <i class="fa fa-angle-right"></i></button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget twitter-widget">
              <h3>Latest Tweets</h3>
              <div class="cp-widget-content">
                <ul class="tweets">
                  <li><a href="#">@nagababu</a>
                    <div class="tweets_txt">Well designed & good information <span> Vizagupdates.com </span></div>
                  </li>
                  <li><a href="#">@Rajesh</a>
                    <div class="tweets_txt">Well Updated...! Helped me a lot thanks <span>Vizagupdates.com</span></div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget gallery-widget">
              <h3>Gallery</h3>
              <div class="cp-widget-content">
                <div id="sync1" class="owl-carousel">
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-1.jpg" alt=""></div>
                </div>
                <div id="sync2" class="owl-carousel">
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-2.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-3.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-4.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-2.jpg" alt=""></div>
                  <div class="item"><img src="<?php echo $asset_url;?>frontend/images/fg-4.jpg" alt=""></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget contact-widget">
              <h3>Contact Info</h3>
              <div class="cp-widget-content">
                <address>
                <ul>
                  <li> <i class="fa fa-university"></i>
                    <p>Vizag Updates,
                      Sivaji Palem,
					  Visakhapatnam - 530017.
					</p>
                  </li>
                  <li> <i class="fa fa-phone"></i>
                    <p> Phone: 09912238386<br>
                    <!--  Fax: 0800 080 1234 --> </p>
                  </li>
                  <li> <i class="fa fa-facebook"></i>
                    <p> Facebook: <a target="_blank" href="https://www.facebook.com/vizag360/">https://www.facebook.com/vizag360/</a> </p>
                  </li>
                  <li> <i class="fa fa-envelope-o"></i>
                    <p> Email: <a href="mailto:info@materialmag.com">uvizag@gmail.com</a> </p>
                  </li>
                  <li> <i class="fa fa-globe"></i>
                    <p> <a href="#">www.vizagupdates.com</a> </p>
                  </li>
                </ul>
                </address>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-three">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="footer-logo"><img src="<?php echo $asset_url;?>frontend/images/footer-logo.png" alt=""></div>
          </div>
          <div class="col-md-6">
            <ul class="footer-social">
              <li> <a target="_blank" href="https://twitter.com/VizagUpdates"><i class="fa fa-twitter"></i></a> </li>
              <li> <a target="_blank" href="https://www.facebook.com/vizag360/"><i class="fa fa-facebook"></i></a> </li>
              <li> <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a> </li>
              <li> <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a> </li>
              <li> <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a> </li>
              <li> <a target="_blank" href="#"><i class="fa fa-youtube"></i></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-four">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p>All Rights Reserved 2016 © Vizag Updates, Designed & Developed by <a href="http://vizagupdates.com" target="_blank">vizagupdates.com</a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- footer end -->
  
   <!--login pop up starts-->
 <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header login_modal_header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        		<h2 class="modal-title" id="myModalLabel">Login to Your Account</h2>
      		</div>
			  <div id="alert-msg"></div>
      		<div class="modal-body login-modal">
      			<p>Stack Overflow is a question and answer site for professional and enthusiast programmers. It's 100% free, no registration required</p>
      			<br/>
      			<div class="clearfix"></div>
      			<div id='social-icons-conatainer'>
	        		<div class='modal-body-left'>
					<?php echo validation_errors(); ?>
					<?php echo form_open('users/validuser'); ?>
	        			<div class="form-group">
		              		<input type="text" id="login_email" placeholder="Enter your Email" value="" name="login_email" class="form-control login-field">
		              		<i class="fa fa-user login-field-icon"></i>
		            	</div>
		
		            	<div class="form-group">
		            	  	<input type="password" id="password" name="password" placeholder="Password" value="" class="form-control login-field">
		              		<i class="fa fa-lock login-field-icon"></i>
		            	</div>
		
		            	<input type="submit" id="submit" class="btn btn-primary modal-login-btn">
		            	<a href="#" class="login-link text-center" data-toggle="modal" data-target="#forget-modal" data-dismiss="modal">Lost your password?</a>
	        		</div>
					</form>
	        		<div class='modal-body-right'>
	        			<div class="modal-social-icons">
	        				<a href='<?php echo base_url('home/facebook'); ?>' class="btn btn-primary facebook"> <i class="fa fa-facebook modal-icons"></i> Sign In with Facebook </a>
	        				<!--<a href='#' class="btn btn-info twitter"> <i class="fa fa-twitter modal-icons"></i> Sign In with Twitter </a> -->
	        				<a href='<?php echo base_url('home/google'); ?>' class="btn btn-danger google"> <i class="fa fa-google-plus modal-icons"></i> Sign In with Google </a>
	        				<!--<a href='#' class="btn btn-primary linkedin"> <i class="fa fa-linkedin modal-icons"></i> Sign In with Linkedin </a>-->
	        			</div> 
	        		</div>
	        		<div id='center-line'> OR </div>
	        	</div>																												
        		<div class="clearfix"></div>
        		
        		<div class="form-group modal-register-btn">
        			<button class="btn btn-default" data-toggle="modal" data-target="#reg-modal" data-dismiss="modal"> New User Please Register</button>
        		</div>
      		</div>
      		<div class="clearfix"></div>
      		<div class="modal-footer login_modal_footer">
      		</div>
    	</div>
  	</div>
</div>
<?php
$refering_url = !empty(current_url()) ? current_url() : base_url();
if($refering_url == "http://www.vizagupdates.com/vu/home?msg=Activated")
{
	$refering_url = base_url();
}
if(strpos($refering_url, 'newpassword') !== false) {
   $refering_url = base_url();
}


$this->session->set_userdata('url', $refering_url);
?>
<!--login pop up ends--> 

   <!--signup pop up starts-->
 <div class="modal fade" id="reg-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header login_modal_header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        		<h2 class="modal-title" id="myModalLabel">Register</h2>
      		</div>
			  <div id="alert-msg"></div>
      		 <div class="modal-body form after_succes">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-9">
                                <input name="name" placeholder="Enter your name" class="form-control" type="text" id="name">
                                <span class="help-block error_red" id="error_name"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9"> 
                                <input name="email" placeholder="Enter your E-mail" class="form-control" type="email" value="<?php echo set_value('email'); ?>" >
								<span class="help-block error_red" id="error_email"></span>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="******" class="form-control " type="password" >
                                <span class="help-block error_red" id="error_password"></span>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Confirm Password</label>
                            <div class="col-md-9">
                                <input name="cpassword" placeholder="******" class="form-control " type="password">
                                <span class="help-block error_red" id="error_cpassword"></span>
                            </div>
                        </div>                                
                     </div>
                </form>
            </div>
			
			<div class="modal-body form success" style="display:none;">
			<div class="form-group">
				<font color="green" size="5"><i>THANK YOU FOR REGISTERING</i></font>
				<p>Thank you for registering to the Vizag Updates website. You should receive a activation email shortly.</p>
				<img style="height:103px; margin-left: 129px;" src="<?php echo base_url();?>/assets/frontend/images/logo.svg" alt="">
			</div>
			</div>
			
            <div class="modal-footer after_succes">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
			
			
      		<div class="clearfix"></div>
      		<div class="modal-footer login_modal_footer">
      		</div>
    	</div>
	</div>
</div>
<!--signup pop up ends-->



<!--forget pop up starts-->
 <div class="modal fade" id="forget-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header login_modal_header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        		<h2 class="modal-title" id="myModalLabel">Forget password</h2>
      		</div>
			  <div id="forget-msg"></div>
      		 <div class="modal-body form after_succes">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"> 
                    <div class="form-body">
                      
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9"> 
                                <input name="forget_email" placeholder="Enter your E-mail" class="form-control" id="forget_email" type="email" value="<?php echo set_value('email'); ?>" >
								<span class="help-block error_email" id="error_forget_email"></span>
                            </div>
                        </div>
					                               
                     </div>
                </form>
            </div>
			
			<div class="modal-body form forget_success" style="display:none;">
			<div class="form-group">
				<font color="green" size="5"><i>THANK YOU FOR REGISTERING</i></font>
				<p>Thank you for registering to the Vizag Updates website. You should receive a activation email shortly.</p>
				<p>If you need any help using the website please email the Development Office or call them on 09912238386.</p>
				<img style="height:103px; margin-left: 129px;" src="<?php echo base_url();?>/assets/frontend/images/logo.svg" alt="">
			</div>
			</div>
			
            <div class="modal-footer after_succes">
                <button type="submit" id="forget" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
			
			
      		<div class="clearfix"></div>
      		<div class="modal-footer login_modal_footer">
      		</div>
    	</div>
	</div>
</div>

   <!--forget pop up ends-->
