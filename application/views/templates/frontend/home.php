<?php
$CI =& get_instance();
$baseurl = $CI->config->item("base_url"); 
$asset_url = $CI->config->item("asset_url"); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php echo $meta;?>
<?php echo $css;?>

</head>
<body>

<!-- Main Wrapper Start -->
<div id="wrapper" class="wrapper"> 
  
<?php echo $header;?>
  
<?php echo $content;?>
  

 <?php echo $footer;?>

  
</div>
<!-- Main Wrapper End --> 

<?php echo $js;?>


</body>
</html>